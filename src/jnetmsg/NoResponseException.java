package jnetmsg;

import java.io.IOException;

public class NoResponseException extends IOException {

	private static final long serialVersionUID = 1L;

	public NoResponseException() {
		super();
	}
	
	public NoResponseException(String msg) {
		super(msg);
	}
	
	public NoResponseException(Throwable cause) {
		super(cause);
	}
	
	public NoResponseException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
