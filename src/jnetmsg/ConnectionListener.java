package jnetmsg;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.LogSource;
import jnetmsg.log.StaticLogSource;

/**
 * Listens for connections on a port on a local interface.
 * 
 * Implements <code>{@link java.util.Set}&lt;{@link Connection}&gt;</code> for all connections to remote clients
 * that are connected to this server. This set is not externally modifiable.
 * 
 * @see Net#listen(InetSocketAddress)
 * @see Net#listen(String, int)
 * @see Net#listen(int)
 * 
 * @author Ben Allen
 *
 */
public class ConnectionListener extends AbstractSet<Connection> implements Closeable {
	
	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(ConnectionListener.class, "listen");

	public static enum Status {
		STARTING, STARTED, STOPPING, STOPPED
	}
	
	public static interface StatusCallback {
		public void execute(ConnectionListener s);
	}
	
	private final SelectionKey key;	
	private final Protocol proto;
	private final Set<Connection> clients = new CopyOnWriteArraySet<>();
	private final AtomicBoolean closed = new AtomicBoolean(false);
	
	private volatile Status status = Status.STARTING;
	
	// only used from net thread
	private final Set<Connection.StatusCallback> connection_callbacks = new HashSet<>();
	private final Set<StatusCallback> status_callbacks = new HashSet<>();
	private final ArrayList<PortMapping> portmaps = new ArrayList<>();
	
	// cached addresses
	private volatile InetSocketAddress local_address;
	
	private final GatewayDiscovery.GatewayCallback gateway_cb = (Gateway gate) -> {
		if (!closed.get() && gate.available()) {
			int port = localAddress().getPort();
			portmaps.add(gate.mapPortTCP(port));
		}
	};
	
	private final DynamicLogSource logger = LOGGER.withHook(LogHook.appendDynamicString(" [%s]", this::description));
	
	ConnectionListener(SelectionKey key_, Protocol proto_) throws IOException {
		key = key_;
		proto = proto_;
		if (channel().isOpen()) {
			// should be already bound
			local_address = (InetSocketAddress) channel().getLocalAddress();
			Net.opened(this);
			// Status.STARTING isn't actually used
			status(Status.STARTED);
			logger.write(LogCategory.TRACE, "listening");
		}
	}
	
	private void execCallback(StatusCallback cb) {
		try {
			cb.execute(this);
		} catch (RuntimeException e) {
			logger.write(LogCategory.ERROR, e, "exception in status callback");
		}
	}
	
	private void execCallback(Connection.StatusCallback cb, Connection con) {
		try {
			cb.execute(con);
		} catch (RuntimeException e) {
			logger.write(LogCategory.ERROR, e, "exception in connection callback");
		}
	}
	
	SelectionKey key() {
		return key;
	}
	
	ServerSocketChannel channel() {
		return (ServerSocketChannel) key.channel();
	}
	
	/**
	 * @return Status of this server.
	 */
	public Status status() {
		return status;
	}
	
	public String description() {
		return local_address == null ? "???" : local_address.toString();
	}
	
	public LogSource logger() {
		return logger;
	}
	
	/**
	 * @return Local socket address of this connection
	 * @throws IOException
	 */
	public InetSocketAddress localAddress() {
		return local_address;
	}
	
	public void mapGatewayPorts() {
		if (Net.gateways() != null) Net.gateways().addGatewayCallback(gateway_cb);
	}
	
	@Override
	public Iterator<Connection> iterator() {
		return clients.iterator();
	}

	@Override
	public int size() {
		return clients.size();
	}
	
	@Override
	public boolean contains(Object o) {
		return clients.contains(o);
	}
	
	/**
	 * Add a connection callback to this server. The callback will be called
	 * whenever this server accepts a new connection or an existing connection
	 * to this server is disconnected.
	 * 
	 * The callback will be called as soon as possible for all connections currently
	 * open to this server.
	 * 
	 * Connection callbacks are always called from the Net worker thread.
	 * 
	 * @param cb Callback to add
	 */
	public void addConnectionCallback(Connection.StatusCallback cb) {
		// doing this on the net thread avoids race conditions with accept()
		Net.invoke(() -> {
			if (connection_callbacks.add(cb)) {
				for (Connection c : clients) {
					execCallback(cb, c);
				}
			}
		});
	}
	
	/**
	 * Remove a connection callback.
	 * 
	 * @param cb Callback to remove
	 */
	public void removeConnectionCallback(Connection.StatusCallback cb) {
		Net.invoke(() -> {
			connection_callbacks.remove(cb);
		});
	}
	
	private void fireConnectionCallbacks(Connection c) {
		for (Connection.StatusCallback cb : connection_callbacks) {
			execCallback(cb, c);
		}
	}
	
	/**
	 * Add a status callback to this server. The callback will be called
	 * whenever the status of the server is changed.
	 * 
	 * If the status when the callback is added is not {@link Status#STARTING},
	 * the callback is called as soon as possible.
	 * 
	 * Status callbacks are always called from the Net worker thread.
	 * 
	 * @param cb Callback to add
	 */
	public void addStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			if (status_callbacks.add(cb) && status != Status.STARTING) {
				execCallback(cb);
			}
		});
	}
	
	/**
	 * Remove a status callback.
	 * 
	 * @param cb Callback to remove
	 */
	public void removeStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			status_callbacks.remove(this);
		});
	}
	
	/**
	 * Receive a packet from the first connected client with a packet waiting
	 * to be received.
	 * 
	 * If no complete packets are waiting to be received, returns null.
	 * 
	 * @see Connection#receive
	 * 
	 * @return Received packet or null
	 */
	public Packet receive() throws IOException {
		for (Connection c : clients) {
			Packet p = c.receive();
			if (p != null) return p;
		}
		return null;
	}
	
	/**
	 * Send a packet to all connected clients.
	 * TODO rename to broadcast
	 * 
	 * @see Connection#send
	 * 
	 * @param p Packet to send
	 */
	public void send(Packet p) throws IOException {
		for (Connection c : clients) {
			c.send(p);
		}
	}
	
	/**
	 * Close this server. No further connections will be accepted.
	 * 
	 * Has no effect on connections that have already been accepted.
	 * 
	 */
	@Override
	public void close() {
		if (closed.getAndSet(true)) return;
		if (Net.gateways() != null) Net.gateways().removeGatewayCallback(gateway_cb);
		Net.invoke(this::cleanup);
	}

	private void status(Status s) {
		Status old = status;
		status = s;
		if (s != old) {
			for (StatusCallback cb : status_callbacks) {
				execCallback(cb);
			}
		}
	}
	
	private void cleanup() {
		status(Status.STOPPING);
		if (channel().isOpen()) {
			logger.write(LogCategory.TRACE, "stop listening");
		}
		try {
			key.cancel();
			channel().close();
		} catch (Exception e) { }
		for (PortMapping pm : portmaps) {
			pm.close();
		}
		Net.closed(this);
		status(Status.STOPPED);
	}
	
	void accept() {
		ServerSocketChannel sch = channel();
		try {
			SocketChannel ch = sch.accept();
			logger.write(LogCategory.TRACE, "accept from %s", ch.getRemoteAddress());
			ch.configureBlocking(false);
			SelectionKey k2 = ch.register(key.selector(), SelectionKey.OP_READ);
			Connection c = new Connection(k2, (InetSocketAddress) ch.getRemoteAddress(), proto, this);
			// ensure proper connection init (protocol handler etc)
			c.connect();
			clients.add(c);
			fireConnectionCallbacks(c);
			c.addStatusCallback((Connection d) -> {
				if (d.status() == Connection.Status.DISCONNECTED) {
					fireConnectionCallbacks(d);
					clients.remove(d);
				}
			});
		} catch (IOException e) {
			logger.write(LogCategory.ERROR, "error accepting: %s", e.getMessage());
		}
	}
	
	@Override
	public int hashCode() {
		return key.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (o.getClass() != getClass()) return false;
		return key.equals(((ConnectionListener) o).key);
	}
	
	@Override
	public String toString() {
		return "Server[" + description() + "; " + clients.size() + " clients]";
	}
}
