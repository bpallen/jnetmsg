package jnetmsg;

public class SimplePacketProtocol implements Protocol {

	@Override
	public String name() {
		return "simplepacketprotocol";
	}

	@Override
	public ProtocolHandler newHandlerInstance(Connection con_) {
		return new SimplePacketProtocolHandler(con_);
	}

}
