package jnetmsg;

public interface Closeable {

	public void close();
	
	default void expediteClosure() {}
	
}
