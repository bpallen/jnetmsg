package jnetmsg;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.StandardSocketOptions;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;

import jnetmsg.http.HTTPProtocol;
import jnetmsg.http.HTTPSProtocol;
import jnetmsg.log.LogCategory;
import jnetmsg.log.StaticLogSource;
import jnetmsg.nntp.NNTPProtocol;
import jnetmsg.nntp.NNTPSProtocol;

/**
 * Net system. Listens on ports, connects to remote hosts and sends packets between them.
 * 
 * @author Ben Allen
 *
 */
public class Net {
	
	public static final StaticLogSource LOGGER = new StaticLogSource(Net.class, "net");

	/**
	 * Default port for service discovery (5891).
	 */
	public static final int DEFAULT_DISCOVERY_PORT = 5891;
	
	static abstract class ReadWriteable {
		abstract void read();
		abstract void write();
		void connect() {}
	}
	
	private static final Thread worker = new Thread(Net::run);
	private static final BlockingQueue<Runnable> async = new LinkedBlockingQueue<>();
	
	private static final AtomicInteger init_counter = new AtomicInteger(0);
	private static final AtomicBoolean initialized = new AtomicBoolean(false);
	
	// installed protocols
	private static final Map<String, Protocol> protocols = Collections.synchronizedMap(new HashMap<>());
	
	// default ssl context
	private static SSLContext ssl;
	
	// the selector for all net operations
	private static Selector selector = null;
	
	// Channel.register() will block until Selector.select() wakes up
	// this sync object forces the net thread to wait until registration is complete
	private static final Object register_sync = new Object();
	
	// all open things that should later be closed
	private static final List<Closeable> open_things = Collections.synchronizedList(new ArrayList<Closeable>());
	
	// not used by net thread. synchronize when using.
	private static final Map<Integer, ServiceAdvertisement.Proxy> advertisement_proxies = new HashMap<>();
	
	private static GatewayDiscovery gateways;
	
	private Net() {
		throw new AssertionError("not constructible");
	}
	
	/**
	 * This is just a fail-fast as opposed to a proper safety check (synchronize for that).
	 */
	private static void checkInit() {
		if (!initialized.get()) throw new AssertionError("Net is not initialized, failing fast");
	}
	
	private static void installProtocolInternal(Protocol proto) {
		protocols.put(proto.name(), proto);
	}
	
	private static SSLParameters defaultSSLParametersInternal() {
		SSLParameters p = ssl.getDefaultSSLParameters();
		p.setEndpointIdentificationAlgorithm("HTTPS");
		return p;
	}
	
	/**
	 * Initialize the net system.
	 * Must be called by the application before any other methods in this class.
	 * May be called multiple times; each successful (non-throwing) invocation must be
	 * matched by a called to {@link #close()} before closure will occur.
	 * The supplied logger will only be installed if not null and this is the first call to init.
	 * 
	 * @param logger_ {@link Logger} to install or null
	 */
	public static synchronized void init() throws IOException {
		int c = init_counter.getAndIncrement();
		if (c < 0) throw new IllegalStateException("Attempted to re-init Net");
		if (c == 0) {
			// responsible for init
			selector = Selector.open();
			worker.setDaemon(true);
			worker.setName("Net");
			worker.start();
			try {
				ssl = SSLContext.getInstance("TLS");
				ssl.init(null, null, null);
			} catch (NoSuchAlgorithmException e) {
				LOGGER.write(LogCategory.ERROR, "no TLS implementation available");
			} catch (KeyManagementException e) {
				LOGGER.write(LogCategory.ERROR, "failed to init TLS: %s", e.getMessage());
			}
			installProtocolInternal(new HTTPProtocol());
			installProtocolInternal(new NNTPProtocol());
			if (ssl != null) {
				installProtocolInternal(new HTTPSProtocol(ssl, defaultSSLParametersInternal(), protocols.get("http")));
				installProtocolInternal(new NNTPSProtocol(ssl, defaultSSLParametersInternal(), protocols.get("nntp")));
			}
			initialized.set(true);
		}
	}
	
	/**
	 * Close the net system cleanly. Closes all open advertisements, discoveries,
	 * listeners, connections and executors, and waits for all network IO to complete.
	 * {@link Closeable#close()} is called in reverse order of {@link #opened(Closeable)}.
	 * Perhaps most importantly, removes any UPnP port mappings that were installed.
	 * If this is not called, <b>UPnP port mappings will not be removed!</b>
	 * Does not wait for the application to receive all waiting packets.
	 * Must be called once for each successful invocation of {@link #init()} before
	 * closure will happen.
	 */
	public static synchronized void close() {
		if (init_counter.decrementAndGet() == 0) {
			initialized.set(false);
			// make any re-init attempt fail fast
			init_counter.set(Integer.MIN_VALUE);
			LOGGER.write(LogCategory.TRACE, "closing everything");
			// stop timers
			Timer.close();
			// TODO this syncro is a bug waiting to happen
			// init close connections etc.
			// this syncs with opened()
			synchronized (open_things) {
				// close in reverse order of opening
				for (int i = open_things.size(); i -- > 0; ) {
					Closeable c = open_things.get(i);
					c.close();
				}
			}
			// wait for closed
			while (!open_things.isEmpty()) {
				try {
					if (!worker.isAlive()) {
						LOGGER.write(LogCategory.ERROR, "worker terminated prematurely, abandoning closure");
						break;
					}
					Thread.sleep(20);
					synchronized (open_things) {
						for (Closeable c : open_things) {
							c.expediteClosure();
						}
					}
				} catch (InterruptedException e) { }
			}
			// close the selector (this triggers worker exit)
			try {
				selector.close();
				selector = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
			// wait for worker exit
			while (worker.isAlive()) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) { }
			}
		}
	}

	/**
	 * Starts UPnP gateway discovery, constructing the {@link GatewayDiscovery} singleton
	 * returned by {@link #gateways()}. Has no effect if the singleton is already constructed. 
	 */
	public static synchronized void initGatewayDiscovery() throws IOException {
		checkInit();
		if (gateways == null) gateways = discoverGateways();
	}
	
	/**
	 * Install a protocol. If a protocol with the same name is already installed,
	 * it will be overwritten.
	 * 
	 * @param proto
	 */
	public static void installProtocol(Protocol proto) {
		checkInit();
		Objects.requireNonNull(proto);
		installProtocolInternal(proto);
	}
	
	/**
	 * Get an installed protocol by name.
	 * 
	 * @param name
	 * @return {@link Protocol} or null
	 */
	public static Protocol protocol(String name) {
		checkInit();
		return protocols.get(name);
	}
	
	/**
	 * Get the net system's default {@link SSLContext}, implementing TLS.
	 * 
	 * @return Default SSL context
	 */
	public static SSLContext defaultSSLContext() {
		checkInit();
		return ssl;
	}
	
	/**
	 * Get the net system's default {@link SSLParameters}, with HTTPS-style hostname verification.
	 * Returns a new object for each invocation.
	 * 
	 * @return Default SSL parameters
	 */
	public static SSLParameters defaultSSLParameters() {
		checkInit();
		return defaultSSLParametersInternal();
	}
	
	/**
	 * Start listening for connections on the specified local socket address.
	 * 
	 * @param address Local socket address
	 * @return New server
	 * @throws IOException
	 */
	public static ConnectionListener listen(InetSocketAddress address, Protocol proto) throws IOException {
		checkInit();
		ServerSocketChannel ch = ServerSocketChannel.open();
		ch.bind(address);
		ch.configureBlocking(false);
		SelectionKey key;
		synchronized(register_sync) {
			selector.wakeup();
			key = ch.register(selector, SelectionKey.OP_ACCEPT);
		}
		ConnectionListener cs = new ConnectionListener(key, proto);
		key.attach(cs);
		return cs;
	}
	
	/**
	 * Start listening for connections on the specified local interface and port.
	 * 
	 * @param address Local interface
	 * @param port Local port
	 * @return New server
	 * @throws IOException
	 */
	public static ConnectionListener listen(String address, int port, Protocol proto) throws IOException {
		return listen(new InetSocketAddress(InetAddress.getByName(address), port), proto);
	}
	
	/**
	 * Start listening for connections on all local interfaces and the specified port.
	 * 
	 * @param port Local port
	 * @return New server
	 * @throws IOException
	 */
	public static ConnectionListener listen(int port, Protocol proto) throws IOException {
		try {
			return listen(new InetSocketAddress(InetAddress.getByName("0.0.0.0"), port), proto);
		} catch (UnknownHostException e) {
			throw new AssertionError(e);
		}
	}
	
	/**
	 * Open a connection to a remote host. If required, the hostname that the address
	 * was constructed from ({@link InetAddress#getByName(String)}) will be used for
	 * SSL hostname verification.
	 * 
	 * @param address Remote socket address
	 * @return New connection
	 * @throws IOException
	 */
	public static Connection connect(InetSocketAddress address, Protocol proto) throws IOException {
		checkInit();
		SocketChannel ch = SocketChannel.open();
		ch.configureBlocking(false);
		ch.connect(address);
		SelectionKey key;
		synchronized(register_sync) {
			selector.wakeup();
			key = ch.register(selector, SelectionKey.OP_CONNECT);			
		}
		return new Connection(key, address, proto, null);
	}
	
	/**
	 * Open a connection to a remote host by name and port, determining the address
	 * by {@link InetAddress#getByName(String)}. If required, the hostname will be
	 * used for SSL hostname verification.
	 * 
	 * @param hostname Remote hostname
	 * @param port Remote port
	 * @return New connection
	 * @throws IOException
	 */
	public static Connection connect(String hostname, int port, Protocol proto) throws IOException {
		return connect(new InetSocketAddress(InetAddress.getByName(hostname), port), proto);
	}
	
	/**
	 * Open a connection to a discovered local network {@link Service}.
	 * 
	 * @param s Discovered service
	 * @return New connection
	 * @throws IOException 
	 */
	public static Connection connect(Service s, Protocol proto) throws IOException {
		// TODO service advertisement should include protocol
		return connect(s.remoteAddress(), proto);
	}
	
	/**
	 * Open a connection to the remote host specified by an URL.
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static Connection connect(URL url) throws IOException {
		Protocol proto = protocol(url.getProtocol());
		if (proto == null) throw new IOException("no installed protocol for " + url.getProtocol());
		int port = url.getPort();
		if (port < 1) port = url.getDefaultPort();
		if (port < 1) throw new IOException("could not determine port for " + url);
		return connect(url.getHost(), port, proto);
	}
	
	/**
	 * Attempt to discover advertised local network {@link ConnectionListener}s by service name using
	 * the specified discovery port and timeout period. Servers are {@link #advertise}d
	 * on a specific port using a specific service name.
	 * 
	 * @param service_name Service name to discover
	 * @param port Port to use for discovery
	 * @param timeout_millis Time after which discovery will be stopped in milliseconds
	 * @return New service discovery
	 * @throws IOException
	 */
	public static ServiceDiscovery discover(String service_name, int port, int timeout_millis) throws IOException {
		checkInit();
		Objects.requireNonNull(service_name);
		if (service_name.getBytes("UTF-8").length > 100) throw new IllegalArgumentException("Service name is too long");
		InetSocketAddress target = new InetSocketAddress(InetAddress.getByName("255.255.255.255"), port);
		DatagramChannel ch = DatagramChannel.open();
		ch.setOption(StandardSocketOptions.SO_BROADCAST, true);
		ch.configureBlocking(false);
		ch.bind(new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 0));
		SelectionKey key;
		synchronized (register_sync) {
			selector.wakeup();
			key = ch.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
		}
		return new ServiceDiscovery(key, service_name, target, timeout_millis);
	}
	
	/**
	 * Attempt to discover advertised local network {@link ConnectionListener}s by service name using
	 * the default discovery port and specified timeout period. Servers are {@link #advertise}d
	 * on a specific port using a specific service name.
	 * 
	 * @param service_name Service name to discover
	 * @param timeout_millis Time after which discovery will be stopped in milliseconds
	 * @return New service discovery
	 * @throws IOException
	 */
	public static ServiceDiscovery discover(String service_name, int timeout_millis) throws IOException {
		return discover(service_name, DEFAULT_DISCOVERY_PORT, timeout_millis);
	}
	
	/**
	 * Attempt to discover advertised local network {@link ConnectionListener}s by service name using
	 * the default discovery port and timeout period (30s). Servers are {@link #advertise}d
	 * on a specific port using a specific service name.
	 * 
	 * @see #DEFAULT_DISCOVERY_PORT
	 * 
	 * @param service_name Service name to discover
	 * @return New service discovery
	 * @throws IOException
	 */
	public static ServiceDiscovery discover(String service_name) throws IOException {
		return discover(service_name, DEFAULT_DISCOVERY_PORT, 30000);
	}
	
	/**
	 * Advertise a {@link ConnectionListener} on the local network on a specified
	 * discovery port using a specified service name and instance name. Discovery
	 * ({@link #discover}) happens on a specific port by service name, and the
	 * instance name serves to differentiate discovered services.
	 * 
	 * @param listener Listener to advertise
	 * @param port Port to advertise on
	 * @param service_name Service name, e.g. <code>"MyMultiplayerGame"</code>
	 * @param instance_name Instance name, e.g. <code>"my.host.name"</code>
	 * @return New service advertisement
	 * @throws IOException
	 */
	public static ServiceAdvertisement advertise(ConnectionListener listener, int port, String service_name, String instance_name) throws IOException {
		checkInit();
		Objects.requireNonNull(listener);
		Objects.requireNonNull(service_name);
		Objects.requireNonNull(instance_name);
		if (listener.status() != ConnectionListener.Status.STARTED) throw new IllegalArgumentException("Server is not started");
		if (service_name.getBytes("UTF-8").length > 100) throw new IllegalArgumentException("Service name is too long");
		if (instance_name.getBytes("UTF-8").length > 100) throw new IllegalArgumentException("Instance name is too long");
		ServiceAdvertisement.Proxy proxy = null;
		// get or create proxy
		synchronized (advertisement_proxies) {
			proxy = advertisement_proxies.get(port);
			if (proxy == null) {
				// need to create the proxy
				InetSocketAddress address = new InetSocketAddress(InetAddress.getByName("0.0.0.0"), port);
				DatagramChannel ch = DatagramChannel.open();
				ch.configureBlocking(false);
				ch.bind(address);
				SelectionKey key;
				synchronized (register_sync) {
					selector.wakeup();
					key = ch.register(selector, SelectionKey.OP_READ);
				}
				proxy = new ServiceAdvertisement.Proxy(key);
				advertisement_proxies.put(port, proxy);
			}
		}
		return new ServiceAdvertisement(proxy, listener, service_name, instance_name);
	}
	
	/**
	 * Advertise a {@link ConnectionListener} on the local network on a specified
	 * discovery port using a specified service name and instance name. Discovery
	 * ({@link #discover}) happens on a specific port by service name, and the
	 * instance name serves to differentiate discovered services.
	 * 
	 * @see #DEFAULT_DISCOVERY_PORT
	 * 
	 * @param listener Listener to advertise
	 * @param service_name Service name, e.g. <code>"MyMultiplayerGame"</code>
	 * @param instance_name Instance name, e.g. <code>"my.host.name"</code>
	 * @return New service advertisement
	 * @throws IOException
	 */
	public static ServiceAdvertisement advertise(ConnectionListener listener, String service_name, String instance_name) throws IOException {
		return advertise(listener, DEFAULT_DISCOVERY_PORT, service_name, instance_name);
	}
	
	/**
	 * Discovers UPnP gateways. Gateway discovery does not start (and this method returns null)
	 * until {@link #startGatewayDiscovery()} has been successfully called.
	 * 
	 * @return {@link GatewayDiscovery} singleton 
	 */
	public static GatewayDiscovery gateways() {
		return gateways;
	}
	
	public static void invoke(Runnable r) {
		async.offer(r);
		selector.wakeup();
	}
	
	static GatewayDiscovery discoverGateways() throws IOException {
		checkInit();
		Gateway.staticInit();
		// explicitly open a socket on each interface to facilitate working out
		// which local interface we received each datagram on (needed to setup port forwarding)
		ArrayList<SelectionKey> keys = new ArrayList<>();
		for (Enumeration<NetworkInterface> it = NetworkInterface.getNetworkInterfaces(); it.hasMoreElements(); ) {
			for (InterfaceAddress a : it.nextElement().getInterfaceAddresses()) {
				if (a.getAddress().isSiteLocalAddress()) {
					DatagramChannel ch = DatagramChannel.open();
					ch.setOption(StandardSocketOptions.SO_BROADCAST, true);
					ch.configureBlocking(false);
					ch.bind(new InetSocketAddress(a.getAddress(), 0));
					SelectionKey key;
					synchronized (register_sync) {
						selector.wakeup();
						key = ch.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
					}
					keys.add(key);
				}
			}
		}
		return new GatewayDiscovery(keys, 30000);
	}
	
	static void opened(Closeable c) {
		synchronized (open_things) {
			// test flag within sync block to sync with close() properly
			checkInit();
			// TODO this could be an issue if we had looots of things open
			if (open_things.contains(c)) return;
			open_things.add(c);
		}
	}
	
	static void closed(Closeable c) {
		open_things.remove(c);
	}
	
	private static void runAsync() {
		// run async tasks
		Runnable r = null;
		while ((r = async.poll()) != null) {
			try {
				r.run();
			} catch (CancelledKeyException e) {
				// suppress
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private static void run() {
		LOGGER.write(LogCategory.TRACE, "net worker starting");
		// ensure timer thread started
		Timer.invoke(() -> {});
		try {
			while (true) {
				// wait for an event
				do {
					runAsync();
					// wait for registration to complete before reselecting
					synchronized(register_sync) { }
				} while (selector.select() == 0);
				// do events
				Iterator<SelectionKey> it = selector.selectedKeys().iterator();
				while (it.hasNext()) {
					SelectionKey key = it.next();
					if (key.attachment() != null) {
						try {
							// perform actual IO operations
							if (key.isAcceptable()) ((ConnectionListener) key.attachment()).accept();
							if (key.isConnectable()) ((ReadWriteable) key.attachment()).connect();
							if (key.isReadable()) ((ReadWriteable) key.attachment()).read();
							if (key.isWritable()) ((ReadWriteable) key.attachment()).write();
						} catch (CancelledKeyException e) {
							// a key was used after being cancelled
						}
					}
					// deselect key
					it.remove();
				}
			}
			
		} catch (ClosedSelectorException e) {
			LOGGER.write(LogCategory.TRACE, "net worker terminating");
			// do any remaining tasks
			runAsync();
		} catch (Exception e) {
			LOGGER.write(LogCategory.ERROR, e, "net worker terminating because of error");
		}
		
	}
}
