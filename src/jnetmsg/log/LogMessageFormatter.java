package jnetmsg.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public interface LogMessageFormatter {
	
	public static final LogMessageFormatter DEFAULT_FORMATTER = (LogMessage msg) -> {
		String s = String.format("%s %8s [%s] %s", DateTimeFormatter.ISO_INSTANT.format(msg.time.truncatedTo(ChronoUnit.MILLIS)), msg.category.name, msg.source.fullName(), msg.message);
		if (msg.exception != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			pw.println(s);
			msg.exception.printStackTrace(pw);
			pw.flush();
			return sw.toString();
		} else {
			return s;
		}
	};
	
	String formatMessage(LogMessage msg);
}
