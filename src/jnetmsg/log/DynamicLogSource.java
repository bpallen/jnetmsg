package jnetmsg.log;

import java.util.Objects;

public class DynamicLogSource implements LogSource {

	private final StaticLogSource inner;
	private final LogHook hook;
	
	public DynamicLogSource(StaticLogSource inner_, LogHook hook_) {
		if (inner_.parent() == null) throw new IllegalArgumentException("can't bind hook to root log source");
		inner = Objects.requireNonNull(inner_);
		hook = hook_;
	}

	@Override
	public StaticLogSource parent() {
		return inner.parent();
	}

	@Override
	public StaticLogSource inner() {
		return inner;
	}

	@Override
	public Class<?> origin() {
		return inner.origin();
	}

	@Override
	public String name() {
		return inner.name();
	}

	@Override
	public LogHook hook() {
		return hook;
	}
	
	@Override
	public DynamicLogSource withHook(LogHook hook) {
		return new DynamicLogSource(this.inner, LogHook.chain(hook, this.hook));
	}

	@Override
	public void submit(LogSubmission sub) {
		if (hook != null) hook.logHook(sub);
		inner.submit(sub);
	}

	@Override
	public void flush() {
		inner.flush();
	}
}
