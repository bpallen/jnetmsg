package jnetmsg.log;

public enum LogSeverity {
	INFO, WARNING, ERROR
}
