package jnetmsg.log;

import java.util.function.Supplier;

public interface LogHook {
	
	void logHook(LogSubmission sub);
	
	public static LogHook chain(LogHook hook1, LogHook hook2) {
		if (hook1 == null) return hook2;
		if (hook2 == null) return hook1;
		return (LogSubmission sub) -> {
			hook1.logHook(sub);
			hook2.logHook(sub);
		};
	}
	
	public static LogHook appendString(String s) {
		return (LogSubmission sub) -> {
			sub.message.append(s);
		};
	}
	
	public static LogHook appendString(String fmt, Object... args) {
		final String s = String.format(fmt, args);
		return (LogSubmission sub) -> {
			sub.message.append(s);
		};
	}
	
	@SafeVarargs
	public static LogHook appendDynamicString(String fmt, Supplier<Object>... args) {
		return (LogSubmission sub) -> {
			Object[] args2 = new Object[args.length];
			for (int i = 0; i < args.length; i++) {
				try {
					args2[i] = args[i].get();
				} catch (Exception e) {
					//e.printStackTrace();
					args2[i] = "?";
				}
			}
			sub.message.append(String.format(fmt, args2));
		};
	}
}
