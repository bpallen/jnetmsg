package jnetmsg.log;

import java.time.Instant;

public class LogMessage {

	public final LogSource source;
	public final LogCategory category;
	public final Exception exception;
	public final Instant time;
	public final Thread thread;
	public final String message;
	
	public LogMessage(LogSubmission sub) {
		source = sub.source;
		category = sub.category;
		exception = sub.exception;
		time = Instant.now();
		thread = Thread.currentThread();
		message = sub.message.toString();
	}

}
