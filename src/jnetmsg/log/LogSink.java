package jnetmsg.log;

import java.io.Closeable;
import java.io.OutputStreamWriter;

public interface LogSink extends Closeable {

	public static final AbstractTextLogSink STDERR = new WriterLogSink(new OutputStreamWriter(System.err)); 
	
	default int verbosity() {
		return 9001;
	}
	
	default void submit(LogMessage msg) {
		if (msg.category.verbosity > verbosity()) return;
		try {
			submitImpl(msg);
		} catch (Exception e) {
			// TODO sink failure handling
		}
	}
	
	public void submitImpl(LogMessage msg); 
	
	public void flush();
	
	public void notifyAttach(StaticLogSource src);
	
	public void notifyDetach(StaticLogSource src);
	
}
