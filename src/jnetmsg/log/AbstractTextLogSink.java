package jnetmsg.log;

import java.util.Objects;

public abstract class AbstractTextLogSink extends AbstractLogSink {

	private volatile LogMessageFormatter formatter = LogMessageFormatter.DEFAULT_FORMATTER;
	
	protected String formatMessage(LogMessage msg) {
		return formatter.formatMessage(msg);
	}
	
	protected void setFormatter(LogMessageFormatter f) {
		formatter = Objects.requireNonNull(f);
	}

}
