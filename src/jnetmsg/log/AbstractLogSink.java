package jnetmsg.log;

import java.io.IOException;
import java.util.ArrayList;

public abstract class AbstractLogSink implements LogSink {
	
	private final ArrayList<StaticLogSource> sources = new ArrayList<>();
	private volatile int verbosity = 9001;

	@Override
	public int verbosity() {
		return verbosity;
	}
	
	public void verbosity(int v) {
		verbosity = v;
	}
	
	@Override
	public synchronized void notifyAttach(StaticLogSource src) {
		sources.add(src);
	}

	@Override
	public synchronized void notifyDetach(StaticLogSource src) {
		sources.remove(src);
	}

	@Override
	public synchronized void close() throws IOException {
		StaticLogSource sources[] = this.sources.toArray(new StaticLogSource[this.sources.size()]);
		this.sources.clear();
		for (StaticLogSource src : sources) {
			src.detach(this);
		}
	}

}
