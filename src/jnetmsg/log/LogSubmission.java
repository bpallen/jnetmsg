package jnetmsg.log;

import java.util.Objects;

public class LogSubmission {

	public final LogSource source;
	public final LogCategory category;
	public final Exception exception;
	public final StringBuilder message = new StringBuilder();
	
	public LogSubmission(LogSource source_, LogCategory category_, Exception exception_) {
		source = Objects.requireNonNull(source_);
		category = Objects.requireNonNull(category_);
		exception = exception_;
	}

	public void format(String fmt, Object... args) {
		message.append(String.format(fmt, args));
	}
	
}
