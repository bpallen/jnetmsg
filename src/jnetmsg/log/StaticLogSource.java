package jnetmsg.log;

import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

public class StaticLogSource implements LogSource {

	public static final StaticLogSource ROOT = new StaticLogSource();
	
	private final StaticLogSource parent;
	private final Class<?> origin;
	private final String name;
	
	private volatile CopyOnWriteArrayList<LogSink> sinks;
	
	private StaticLogSource() {
		origin = StaticLogSource.class;
		parent = null;
		name = "";
	}
	
	public StaticLogSource(Class<?> origin_, String name_) {
		parent = ROOT;
		origin = Objects.requireNonNull(origin_);
		name = Objects.requireNonNull(name_);
	}
	
	public StaticLogSource(StaticLogSource parent_, Class<?> origin_, String name_) {
		parent = Objects.requireNonNull(parent_);
		origin = Objects.requireNonNull(origin_);
		name = Objects.requireNonNull(name_);
	}
	
	@Override
	public StaticLogSource parent() {
		return parent;
	}
	
	@Override
	public StaticLogSource inner() {
		return this;
	}
	
	@Override
	public Class<?> origin() {
		return origin;
	}
	
	@Override
	public String name() {
		return name;
	}
	
	@Override
	public DynamicLogSource withHook(LogHook hook) {
		return new DynamicLogSource(this, hook);
	}
	
	public StaticLogSource newChild(Class<?> origin_, String name_) {
		return new StaticLogSource(this, origin_, name_);
	}
	
	@Override
	public void submit(LogSubmission sub) {
		if (sub.source.inner() != this) throw new IllegalArgumentException("bad log source");
		submit(new LogMessage(sub));
	}
	
	protected void submit(LogMessage msg) {
		Objects.requireNonNull(msg);
		final CopyOnWriteArrayList<LogSink> sinks = this.sinks;
		if (sinks != null) {
			for (LogSink sink : sinks) {
				sink.submit(msg);
			}
		}
		if (parent != null) parent.submit(msg);
	}
	
	@Override
	public void flush() {
		final CopyOnWriteArrayList<LogSink> sinks = this.sinks;
		if (sinks != null) {
			for (LogSink sink : sinks) {
				sink.flush();
			}
		}
		if (parent != null) parent.flush();
	}
	
	public synchronized void attach(LogSink sink) {
		if (sinks == null) sinks = new CopyOnWriteArrayList<>();
		if (sinks.addIfAbsent(sink)) sink.notifyAttach(this);
	}
	
	public synchronized void detach(LogSink sink) {
		if (sinks == null) return;
		if (sinks.remove(sink)) sink.notifyDetach(this);
	}
}
