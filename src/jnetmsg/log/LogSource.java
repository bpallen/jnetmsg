package jnetmsg.log;

public interface LogSource {

	public StaticLogSource parent();
	
	public StaticLogSource inner();
	
	public Class<?> origin();
	
	public String name();
	
	default String fullName() {
		return parent() == null ? name() : parent().fullName() + "." + name();
	}
	
	default LogHook hook() {
		return null;
	}
	
	public DynamicLogSource withHook(LogHook hook);
	
	public void submit(LogSubmission sub);
	
	public void flush();
	
	default void write(LogCategory cat, String fmt, Object... args) {
		LogSubmission sub = new LogSubmission(this, cat, null);
		sub.format(fmt, args);
		submit(sub);
	}
	
	default void write(LogCategory cat, Exception ex, String fmt, Object... args) {
		LogSubmission sub = new LogSubmission(this, cat, ex);
		sub.format(fmt, args);
		submit(sub);
	}
}
