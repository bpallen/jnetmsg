package jnetmsg.log;

public class LogCategory {

	public static final LogCategory DEBUG = new LogCategory("debug", LogSeverity.INFO, 5);
	public static final LogCategory TRACE = new LogCategory("trace", LogSeverity.INFO, 4);
	public static final LogCategory STATUS = new LogCategory("status", LogSeverity.INFO, 3);
	public static final LogCategory WARNING = new LogCategory("warning", LogSeverity.INFO, 2);
	public static final LogCategory ERROR = new LogCategory("error", LogSeverity.INFO, 1);
	public static final LogCategory CRITICAL = new LogCategory("critical", LogSeverity.INFO, 0);
	
	public final String name;
	public final LogSeverity severity;
	public final int verbosity;
	
	public LogCategory(String name_, LogSeverity severity_, int verbosity_) {
		name = name_;
		severity = severity_;
		verbosity = verbosity_;
	}

}
