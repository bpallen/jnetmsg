package jnetmsg.log;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Objects;

public class WriterLogSink extends AbstractTextLogSink {

	// NOTE we dont own the output, so we dont close it!
	private final PrintWriter out;
	
	public WriterLogSink(Writer out_) {
		out = new PrintWriter(Objects.requireNonNull(out_));
	}

	@Override
	public synchronized void submitImpl(LogMessage msg) {
		out.println(formatMessage(msg));
		out.flush();
	}

	@Override
	public synchronized void flush() {
		out.flush();
	}
	
}
