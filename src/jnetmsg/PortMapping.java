package jnetmsg;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.atomic.AtomicBoolean;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.StaticLogSource;

public class PortMapping implements Closeable {

	public static final StaticLogSource LOGGER = Gateway.LOGGER.newChild(PortMapping.class, "portmapping");
	
	public static enum Status {
		/** Not resolved yet */
		UNKNOWN,
		/** Not mappable; possibly in use or mapped elsewhere */
		UNAVAILABLE,
		/** Available to be mapped */
		AVAILABLE,
		/** Mapped to us (on the same port) */
		MAPPED
	}

	private final DynamicLogSource logger = LOGGER.withHook(LogHook.appendDynamicString(" [%s]", this::description));
	private final Gateway gate;
	private final InetSocketAddress client;
	private final String proto;
	private final AtomicBoolean closed = new AtomicBoolean(false);
	private volatile Status status = Status.UNKNOWN;
	
	PortMapping(Gateway gate_, InetSocketAddress client_, String proto_) {
		gate = gate_;
		client = client_;
		proto = proto_;
		Gateway.invoke(() -> {
			try {
				gate.soapAddPortMapping(client, proto);
				status(Status.MAPPED);
				logger.write(LogCategory.TRACE, "added port mapping on gateway");
			} catch (Exception e) {
				status(Status.UNAVAILABLE);
				logger.write(LogCategory.ERROR, "failed to add port mapping on gateway: %s", e.getMessage());
			}
		});
		Net.opened(this);
	}
	
	public String description() {
		return String.format("%s -> %s", gate.address(), client);
	}
	
	private void status(Status s) {
		// TODO status callbacks
		status = s;
	}
	
	public Status status() {
		return status;
	}
	
	private void cleanup() {
		if (status == Status.MAPPED) {
			try {
				gate.soapDeletePortMapping(client.getPort(), proto);
				logger.write(LogCategory.TRACE, "removed port mapping on gateway");
			} catch (IOException e) {
				logger.write(LogCategory.ERROR, "failed to remove port mapping on gateway: %s", e.getMessage());
			}
		}
		Net.closed(this);
		status(Status.UNKNOWN);
	}
	
	@Override
	public void close() {
		if (closed.getAndSet(true)) return;
		Gateway.invoke(this::cleanup);
	}
	
}
