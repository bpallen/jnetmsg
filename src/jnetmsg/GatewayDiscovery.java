package jnetmsg;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;

import jnetmsg.log.LogCategory;
import jnetmsg.log.StaticLogSource;

/**
 * TODO document this
 * 
 * @author Ben Allen
 *
 */
public class GatewayDiscovery extends AbstractSet<Gateway> implements Closeable {

	public static final StaticLogSource LOGGER = Gateway.LOGGER.newChild(GatewayDiscovery.class, "discover");
	
	// this is the key attachment
	private class ReadWriteable extends Net.ReadWriteable {
		
		public final SelectionKey key;
		
		// only used on the net thread
		public final Queue<Datagram> pending_datagrams = new LinkedList<>();
		
		public ReadWriteable(SelectionKey key_) {
			key = key_;
		}
		
		public DatagramChannel channel() {
			return (DatagramChannel) key.channel();
		}
		
		@Override
		void read() {
			GatewayDiscovery.this.read(this);
		}

		@Override
		void write() {
			GatewayDiscovery.this.write(this);
		}
	}
	
	public static enum Status {
		STARTING, STARTED, STOPPING, STOPPED
	}
	
	public static interface GatewayCallback {
		public void execute(Gateway gate);
	}
	
	public static interface StatusCallback {
		public void execute(GatewayDiscovery disco);
	}
	
	private static class Datagram {
		
		public final InetSocketAddress host;
		public final ByteBuffer data;
		
		public Datagram(InetSocketAddress host_, ByteBuffer data_) {
			host = host_;
			data = data_;
		}
	}
	
	// delay between broadcasts in millis
	private static final int DELAY = 5000;
	
	// SSDP search target strings
	private static final String[] search_targets = new String[] {
        "urn:schemas-upnp-org:device:InternetGatewayDevice:1",
        "urn:schemas-upnp-org:service:WANIPConnection:1",
        "urn:schemas-upnp-org:service:WANPPPConnection:1"
	};
	
	private final StaticLogSource logger = LOGGER;
	
	private final AtomicBoolean closed = new AtomicBoolean(false);
	private volatile Status status = Status.STARTING;
	private final Set<Gateway> gateways = new CopyOnWriteArraySet<>();
	
	// list never modified after population; datagram queues are only used on the net thread
	private final List<ReadWriteable> readwrite = new ArrayList<>();
	
	// only used on net thread
	private final Set<GatewayCallback> gateway_callbacks = new HashSet<>();
	private final Set<StatusCallback> status_callbacks = new HashSet<>();
	private final Map<InetSocketAddress, InetAddress> targets = new HashMap<>(); 
	
	private ByteBuffer in_buf = ByteBuffer.allocate(1500);
	
	GatewayDiscovery(List<SelectionKey> keys_, int timeout_millis_) throws IOException {
		try {
			// start with the SSDP broadcast address
			addTarget(InetAddress.getByName("239.255.255.250"), null);
		} catch (UnknownHostException e) {
			throw new AssertionError(e);
		}
		// generate key attachments
		for (SelectionKey key : keys_) readwrite.add(new ReadWriteable(key));
		// only attach after completing readwrite list (prevent premature read/write)
		for (ReadWriteable rw : readwrite) rw.key.attach(rw);
		logger.write(LogCategory.TRACE, "start discovering gateways on %s", localAddresses());
		// periodically broadcast a service discovery message
		Timer.add(DELAY, true, (long elapsed) -> {
			// regenerate search datagrams
			Net.invoke(() -> {
				for (ReadWriteable rw : readwrite) rw.pending_datagrams.clear();
				for (InetSocketAddress t : targets.keySet()) {
					for (String s : search_targets) {
						ByteBuffer req = getHTTPRequest(s, t);
						for (ReadWriteable rw : readwrite) {
							rw.pending_datagrams.offer(new Datagram(t, req));
						}
					}
				}
			});
			interestWrite();
			// close after timeout
			if (elapsed >= timeout_millis_) {
				close();
				return false;
			}
			return true;
		});
		Net.opened(this);
	}
	
	private void execCallback(StatusCallback cb) {
		try {
			cb.execute(this);
		} catch (RuntimeException e) {
			logger.write(LogCategory.ERROR, e, "exception in status callback");
		}
	}
	
	private void execCallback(GatewayCallback cb, Gateway g) {
		try {
			cb.execute(g);
		} catch (RuntimeException e) {
			logger.write(LogCategory.ERROR, e, "exception in discovery callback");
		}
	}
	
	private ByteBuffer getHTTPRequest(String search, InetSocketAddress target) {
		String text = String.format(
			"M-SEARCH * HTTP/1.1\r\n"
			+ "HOST: %s:%d\r\n"
			+ "MAN: \"ssdp:discover\"\r\n"
			+ "MX: %d\r\n"
			+ "ST: %s\r\n"
			+ "\r\n",
			target.getAddress().getHostAddress(), target.getPort(), DELAY / 1000, search
		);
		byte[] data = text.getBytes();
		return ByteBuffer.wrap(data);
	}
	
	public void addTarget(InetAddress target, InetAddress forward) {
		Net.invoke(() -> { targets.put(new InetSocketAddress(target, 1900), forward); });
	}
	
	public Status status() {
		return status;
	}
	
	private void status(Status s) {
		Status old = status;
		status = s;
		if (s != old) {
			for (StatusCallback cb : status_callbacks) {
				execCallback(cb);
			}
		}
	}
	
	private void interestWrite() {
		Net.invoke(() -> {
			for (ReadWriteable rw : readwrite) {
				rw.key.interestOps(rw.key.interestOps() | SelectionKey.OP_WRITE);
			}
		});
	}
	
	@Override
	public boolean contains(Object o) {
		return gateways.contains(o);
	}

	@Override
	public Iterator<Gateway> iterator() {
		return gateways.iterator();
	}

	@Override
	public int size() {
		return gateways.size();
	}

	public List<InetSocketAddress> localAddresses() throws IOException {
		ArrayList<InetSocketAddress> l = new ArrayList<>();
		for (ReadWriteable rw : readwrite) {
			l.add((InetSocketAddress) rw.channel().getLocalAddress());
		}
		return l;
	}
	
	public void addGatewayCallback(GatewayCallback cb) {
		Net.invoke(() -> {
			if (gateway_callbacks.add(cb)) {
				for (Gateway g : gateways) {
					execCallback(cb, g);
				}
			}
		});
	}
	
	public void removeGatewayCallback(GatewayCallback cb) {
		Net.invoke(() -> {
			gateway_callbacks.remove(cb);
		});
	}
	
	public void addStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			if (status_callbacks.add(cb) && status != Status.STARTING) {
				execCallback(cb);
			}
		});
	}
	
	public void removeStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			status_callbacks.remove(cb);
		});
	}
	
	private void cleanup() {
		status(Status.STOPPING);
		try {
			logger.write(LogCategory.TRACE, "stop discovering gateways on %s", localAddresses());
		} catch (IOException e1) { }
		try {
			for (ReadWriteable rw : readwrite) {
				rw.key.cancel();
				rw.channel().close();
			}
		} catch (Exception e) { }
		Net.closed(this);
		status(Status.STOPPED);
	}
	
	@Override
	public void close() {
		if (closed.getAndSet(true)) return;
		Net.invoke(this::cleanup);
	}
	
	private static InetAddress outboundAddress(InetSocketAddress remote) {
		try {
			// https://stackoverflow.com/questions/22045165/java-datagrampacket-receive-how-to-determine-local-ip-interface
			DatagramSocket sock = new DatagramSocket();
			// connect is needed to bind the socket and retrieve the local address
			// later (it would return 0.0.0.0 otherwise)
			sock.connect(remote);
			final InetAddress local = sock.getLocalAddress();
			sock.disconnect();
			sock.close();
			return local;
		} catch (SocketException e) {
			return null;
		}
	}
	
	void fireGatewayCallbacks(Gateway gate) {
		Net.invoke(() -> {
			for (GatewayCallback cb : gateway_callbacks) {
				execCallback(cb, gate);
			}
		});
	}
	
	void read(ReadWriteable rw) {
		try {
			InetSocketAddress sender = (InetSocketAddress) rw.channel().receive(in_buf);
			if (sender == null) return;
			in_buf.flip();
			byte[] b = new byte[in_buf.remaining()];
			in_buf.get(b);
			String text = new String(b);
			// determine where to forward from the gateway (usually us)
			// note: we can't look up forwarding addresses like this for things sent multicast
			// (but thats mostly ok, because anything multicast is probably the same subnet) 
			InetAddress forward = targets.get(sender);
			if (forward == null) forward = ((InetSocketAddress) rw.channel().getLocalAddress()).getAddress();
			// if we couldn't resolve our own address, just give up
			if (forward == null) return;
			Gateway g = new Gateway(sender.getAddress(), forward);
			if (!gateways.contains(g)) {
				if (g.init(text)) {
					// the gateway must remain in the set regardless of further init
					// because otherwise, we'll keep trying to init from search responses
					gateways.add(g);
					// fire callbacks from gateway after all loading complete
				}
			}
		} catch (BufferUnderflowException e) {
			// malformed datagram
		} catch (IOException e) {
			try {
				logger.write(LogCategory.ERROR, "error reading gateway response on %s: %s", rw.channel().getLocalAddress(), e.getMessage());
			} catch (IOException e1) {
				logger.write(LogCategory.ERROR, "error reading gateway response: %s", e.getMessage());
			}
			cleanup();
		} finally {
			// always clear buffer
			in_buf.clear();
		}
	}
	
	void write(ReadWriteable rw) {
		Datagram d = rw.pending_datagrams.poll();
		if (d == null) return;
		try {
			rw.channel().send(d.data, d.host);
			if (rw.pending_datagrams.isEmpty()) rw.key.interestOps(rw.key.interestOps() & ~SelectionKey.OP_WRITE);
		} catch (IOException e) {
			try {
				logger.write(LogCategory.ERROR, "error writing gateway request on %s: %s", rw.channel().getLocalAddress(), e.getMessage());
			} catch (IOException e1) {
				logger.write(LogCategory.ERROR, "error writing gateway request: %s", e.getMessage());
			}
			cleanup();
		}
	}
	
	@Override
	public int hashCode() {
		return readwrite.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (o.getClass() != getClass()) return false;
		return readwrite == ((GatewayDiscovery) o).readwrite;
	}
	
	@Override
	public String toString() {
		// TODO more informative?
		return "GatewayDiscovery";
	}
}
