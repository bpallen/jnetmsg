package jnetmsg;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.util.AbstractSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.StaticLogSource;

/**
 * Discovers {@link ConnectionListener}s advertised on the local network by sending discovery requests
 * for a specified service name.
 * 
 * Implements <code>{@link java.util.Set}&lt;{@link Service}&gt;</code> for all discovered services.
 * This set is not externally modifiable.
 * 
 * @see Net#discover(int, String, int)
 * @see Net#discover(String)
 * 
 * @author Ben Allen
 *
 */
public class ServiceDiscovery extends AbstractSet<Service> implements Closeable {
	
	public static final StaticLogSource LOGGER = Service.LOGGER.newChild(ServiceDiscovery.class, "discover");
	
	// this is the key attachment
	private class ReadWriteable extends Net.ReadWriteable {
		@Override
		void read() {
			ServiceDiscovery.this.read();
		}

		@Override
		void write() {
			ServiceDiscovery.this.write();
		}
	}
	
	public static enum Status {
		STARTING, STARTED, STOPPING, STOPPED
	}
	
	// TODO rename this to ServiceCallback
	public static interface DiscoveryCallback {
		public void execute(Service service);
	}
	
	public static interface StatusCallback {
		public void execute(ServiceDiscovery disco);
	}
	
	// delay between broadcasts in millis
	private static final int DELAY = 2000;
	
	private final DynamicLogSource logger = LOGGER.withHook(LogHook.appendDynamicString(" [%s]", this::description));
	private final SelectionKey key;
	private final String service_name;
	private final Set<Service> services = new CopyOnWriteArraySet<>();
	private final AtomicBoolean closed = new AtomicBoolean(false);
	private volatile Status status = Status.STARTING;
	
	// only used on net thread
	private final Set<DiscoveryCallback> discovery_callbacks = new HashSet<>();
	private final Set<StatusCallback> status_callbacks = new HashSet<>();
	private final Set<InetSocketAddress> targets = new HashSet<>();
	private final Queue<InetSocketAddress> pending_targets = new LinkedList<>();
	
	private ByteBuffer out_buf = ByteBuffer.allocate(256);
	private ByteBuffer in_buf = ByteBuffer.allocate(256);
	
	ServiceDiscovery(SelectionKey key_, String service_name_, InetSocketAddress target_, int timeout_millis_) throws IOException {
		key = key_;
		service_name = service_name_;
		targets.add(target_);
		// prepare buffer to send for discovery:
		// [4] : magic number
		// [2] : service name length
		// [x] : service name
		out_buf.putInt(0x65CC3CD3);
		byte[] servicebytes = service_name.getBytes("UTF-8"); 
		out_buf.putShort((short) servicebytes.length);
		out_buf.put(servicebytes);
		out_buf.flip();
		key.attach(new ReadWriteable());
		logger.write(LogCategory.TRACE, "start discovering service '%s'", service_name);
		Timer.add(DELAY, true, (long elapsed) -> {
			// periodically broadcast a service discovery message
			Net.invoke(() -> {
				pending_targets.clear();
				for (InetSocketAddress t : targets) {
					pending_targets.offer(t);
				}
			});
			interestWrite();
			// close after timeout
			if (elapsed >= timeout_millis_) {
				close();
				return false;
			}
			return true;
		});
		Net.opened(this);
		// Status.STARTING isn't actually used
		status(Status.STARTED);
	}
	
	private void execCallback(StatusCallback cb) {
		try {
			cb.execute(this);
		} catch (RuntimeException e) {
			logger.write(LogCategory.ERROR, e, "exception in status callback");
		}
	}
	
	private void execCallback(DiscoveryCallback cb, Service s) {
		try {
			cb.execute(s);
		} catch (RuntimeException e) {
			logger.write(LogCategory.ERROR, e, "exception in discovery callback");
		}
	}

	SelectionKey key() {
		return key;
	}
	
	DatagramChannel channel() {
		return (DatagramChannel) key.channel();
	}
	
	String description() {
		try {
			return localAddress().toString();
		} catch (IOException e) {
			return "?";
		}
	}
	
	public void addTarget(InetSocketAddress t) {
		Net.invoke(() -> targets.add(t));
	}
	
	/**
	 * @return Status of this service discovery
	 */
	public Status status() {
		return status;
	}
	
	private void status(Status s) {
		Status old = status;
		status = s;
		if (s != old) {
			for (StatusCallback cb : status_callbacks) {
				execCallback(cb);
			}
		}
	}
	
	/**
	 * @return Name of the service being discovered
	 */
	public String serviceName() {
		return service_name;
	}
	
	private void interestWrite() {
		Net.invoke(() -> {
			key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
		});
	}
	
	@Override
	public Iterator<Service> iterator() {
		return services.iterator();
	}

	@Override
	public int size() {
		return services.size();
	}
	
	@Override
	public boolean contains(Object o) {
		return services.contains(o);
	}
	
	/**
	 * @return Local socket address of this service discovery
	 * @throws IOException
	 */
	public InetSocketAddress localAddress() throws IOException {
		return (InetSocketAddress) channel().getLocalAddress();
	}
	
	/**
	 * Add a discovery callback to this service discovery. The callback will be called
	 * whenever a new remote service is discovered.
	 * 
	 * The callback will be called as soon as possible for all remote services
	 * so far discovered.
	 * 
	 * Status callbacks are always called from the Net worker thread.
	 * 
	 * @param cb Callback to add
	 */
	public void addDiscoveryCallback(DiscoveryCallback cb) {
		Net.invoke(() -> {
			if (discovery_callbacks.add(cb)) {
				for (Service s : services) {
					execCallback(cb, s);
				}
			}
		});
	}
	
	/**
	 * Remove a discovery callback.
	 * 
	 * @param cb Callback to remove
	 */
	public void removeDiscoveryCallback(DiscoveryCallback cb) {
		Net.invoke(() -> {
			discovery_callbacks.remove(cb);
		});
	}
	
	/**
	 * Add a status callback to this server. The callback will be called
	 * whenever the status of the server is changed.
	 * 
	 * If the status when the callback is added is not {@link Status#STARTING},
	 * the callback is called as soon as possible.
	 * 
	 * Status callbacks are always called from the Net worker thread.
	 * 
	 * @param cb Callback to add
	 */
	public void addStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			if (status_callbacks.add(cb) && status != Status.STARTING) {
				execCallback(cb);
			}
		});
	}
	
	/**
	 * Remove a status callback.
	 * 
	 * @param cb Callback to remove
	 */
	public void removeStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			status_callbacks.remove(cb);
		});
	}
	
	private void cleanup() {
		status(Status.STOPPING);
		logger.write(LogCategory.TRACE, "stop discovering service '%s'", service_name);
		try {
			key.cancel();
			channel().close();
		} catch (Exception e) { }
		Net.closed(this);
		status(Status.STOPPED);
	}
	
	/**
	 * Close this service discovery. It will not longer search for services.
	 */
	@Override
	public void close() {
		if (closed.getAndSet(true)) return;
		Net.invoke(this::cleanup);
	}
	
	void read() {
		DatagramChannel ch = channel();
		try {
			InetSocketAddress sender = (InetSocketAddress) ch.receive(in_buf);
			if (sender == null) return;
			in_buf.flip();
			// check magic number
			if (in_buf.getInt() != 0xD3A276CF) return;
			// read service details
			int port = 0xFFFF & (int) in_buf.getShort();
			byte[] addrbytes = new byte[0xFF & (int) in_buf.get()];
			in_buf.get(addrbytes);
			// if address is unspecified, use datagram sender
			InetAddress addr = InetAddress.getByAddress(addrbytes);
			if (addr.isAnyLocalAddress()) addr = sender.getAddress();
			byte[] servicebytes = new byte[0xFFFF & (int) in_buf.getShort()];
			in_buf.get(servicebytes);
			byte[] instancebytes = new byte[0xFFFF & (int) in_buf.getShort()];
			in_buf.get(instancebytes);
			String newservicename = new String(servicebytes, "UTF-8");
			String newinstancename = new String(instancebytes, "UTF-8");
			// if service name doesn't match, bail
			if (!service_name.equals(newservicename)) return;
			// otherwise, add new service
			Service s = new Service(service_name, newinstancename, new InetSocketAddress(addr, port));
			if (!services.contains(s)) {
				services.add(s);
				// fire callbacks
				for (DiscoveryCallback cb : discovery_callbacks) {
					execCallback(cb, s);
				}
			}
		} catch (BufferUnderflowException e) {
			// malformed datagram, ignore
		} catch (IOException e) {
			logger.write(LogCategory.ERROR, "error reading service response: %s", e.getMessage());
			cleanup();
		} finally {
			// always clear buffer
			in_buf.clear();
		}
	}
	
	void write() {
		InetSocketAddress target = pending_targets.poll();
		if (target == null) return;
		DatagramChannel ch = channel();
		try {
			ch.send(out_buf, target);
			key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
		} catch (IOException e) {
			logger.write(LogCategory.ERROR, "error writing service request: %s", e.getMessage());
			cleanup();
		} finally {
			// always rewind buffer
			out_buf.rewind();
		}
	}
	
	@Override
	public int hashCode() {
		return key.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (o.getClass() != getClass()) return false;
		return key.equals(((ServiceDiscovery) o).key);
	}
	
	@Override
	public String toString() {
		try {
			return "ServiceDiscovery[local " + localAddress() + ", '" + service_name + "', " + services.size() + " services]";
		} catch (IOException e) {
			return "ServiceDiscovery[?]";
		}
	}
}
