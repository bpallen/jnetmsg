package jnetmsg;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.StaticLogSource;

/**
 * Experimental. Base class for wrapper protocol handlers.
 * 
 * @author Ben Allen
 *
 */
public class WrapperProtocolHandler implements ProtocolHandler {
	
	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(WrapperProtocolHandler.class, "wrapper");
	
	private final DynamicLogSource logger;
	private final ProtocolHandler inner;
	private ByteBuffer in_buf;
	private ByteBuffer out_buf;
	private int requested_buf_size;
	private boolean closing = false;
	
	public WrapperProtocolHandler(ProtocolHandler inner_, int buf_size) {
		inner = Objects.requireNonNull(inner_);
		in_buf = ByteBuffer.allocateDirect(buf_size);
		out_buf = ByteBuffer.allocateDirect(buf_size);
		requested_buf_size = buf_size;
		// need to set the out buf to 'fully written'
		out_buf.position(out_buf.limit());
		logger = LOGGER.withHook(inner.connection().logger().hook());
	}
	
	protected boolean closing() {
		return closing;
	}
	
	public ProtocolHandler inner() {
		return inner;
	}
	
	@Override
	public Connection connection() {
		return inner.connection();
	}

	@Override
	public void init() throws IOException {
		inner.init();
	}
	
	@Override
	public boolean hasReadInterest() {
		return in_buf.hasRemaining() || inner.hasReadInterest();
	}
	
	@Override
	public boolean hasWriteInterest() {
		return out_buf.hasRemaining();
	}
	
	@Override
	public boolean hasMoreOutAppData() {
		return out_buf.hasRemaining() || inner.hasMoreOutAppData();
	}

	@Override
	public ByteBuffer outBuf() {
		return out_buf;
	}

	@Override
	public ByteBuffer inBuf() {
		return in_buf;
	}

	@Override
	public void updateOut() throws IOException {
		boolean done = false;
		boolean retry = false;
		while (!done) {
			inner.updateOut();
			// out_buf is normally set for reading (channel write)
			// switch to writing
			out_buf.compact();
			ByteBuffer b = inner.outBuf();
			if (b == null) b = Util.EMPTY_BUFFER;
			if (inner.hasClosedOut()) innerClosedOut();
			try {
				final int opos0 = out_buf.position();
				retry = wrap(out_buf, b);
				final int opos1 = out_buf.position();
				// if wrap _produced_ nothing (to net), break
				if (opos1 <= opos0 && !retry) done = true;
			} finally {
				// switch back to reading
				out_buf.flip();
			}
			// note: resize could change out_buf object
			resizeBuffers(requested_buf_size);
			// repeat until output is full or no more input and not retrying (eg. ssl connect consumes no input)
			if (out_buf.limit() == out_buf.capacity() || (!b.hasRemaining() && !retry)) break;
		}
	}

	@Override
	public void updateIn(int newbytes) throws IOException {
		boolean done = false;
		boolean retry = false;
		while (!done) {
			resizeBuffers(requested_buf_size);
			// in_buf is normally set for writing (channel read)
			// switch to reading
			in_buf.flip();
			ByteBuffer b = inner.inBuf();
			if (b == null) b = Util.EMPTY_BUFFER;
			final int opos0 = b.position();
			try {
				final int ipos0 = in_buf.position();
				retry = unwrap(b, in_buf, newbytes);
				final int ipos1 = in_buf.position();
				newbytes -= ipos1 - ipos0;
				// if unwrap _consumed_ nothing (from net), break
				if (ipos1 <= ipos0 && !retry) done = true;
			} finally {
				// switch back to writing
				in_buf.compact();
			}
			final int opos1 = b.position();
			inner.updateIn(opos1 - opos0);
			// b could be invalidated by a (inner) resize
			b = inner.inBuf();
			if (b == null) b = Util.EMPTY_BUFFER;
			// repeat until no more input or output is full and not retrying (eg. ssl connect produces no output)
			// retry condition here is probably not required, because a no-ouput
			// operation can't magically consume more input by retrying
			if (in_buf.position() == 0 || (!b.hasRemaining() && !retry)) break;
		}
	}
	
	@Override
	public boolean shouldCloseIn() {
		return inner.shouldCloseIn();
	}

	@Override
	public void closeIn() throws IOException {
		// inner must consume as much input as possible before we forward eof
		// test position because in_buf is normally set for writing (channel read)
		// note that there may be unconsumable input depending on the close circumstances
		if (in_buf.position() > 0) updateIn(0);
		inner.closeIn();
	}

	@Override
	public void beginCloseOut() throws IOException {
		inner.beginCloseOut();
		closing = true;
	}
	
	@Override
	public boolean hasClosedOut() {
		return !out_buf.hasRemaining() && inner.hasClosedOut();
	}

	@Override
	public boolean hasDeliveredEOF() {
		return inner.hasDeliveredEOF();
	}

	@Override
	public void cleanup() {
		inner.cleanup();
		if (out_buf.hasRemaining()) {
			logger().write(LogCategory.WARNING, "discarded outgoing data: %d bytes", out_buf.remaining());
		}
	}

	@Override
	public void checkSendable(Packet p) {
		inner.checkSendable(p);
	}

	@Override
	public SequenceNumber issueSequenceNumber(Packet p) {
		return inner.issueSequenceNumber(p);
	}

	protected void innerClosedOut() {
		
	}
	
	/**
	 * 
	 * @param dst
	 * @param src
	 * @return true if a retry should be performed (due to buffer resize etc)
	 * @throws IOException
	 */
	protected boolean wrap(ByteBuffer dst, ByteBuffer src) throws IOException {
		Util.transfer(dst, src);
		return false;
	}
	
	/**
	 * 
	 * @param dst
	 * @param src
	 * @param newbytes
	 * @return true if a retry should be performed (due to buffer resize etc)
	 * @throws IOException
	 */
	protected boolean unwrap(ByteBuffer dst, ByteBuffer src, int newbytes) throws IOException {
		Util.transfer(dst, src);
		return false;
	}

	public int bufferSize() {
		// both buffers are the same size
		return in_buf.capacity();
	}
	
	/**
	 * Resize buffers (enlarge only).
	 * 
	 * @param buf_size
	 * @return true iff buffers enlarged
	 */
	private boolean resizeBuffers(int buf_size) {
		// shrinking not supported
		// both buffers are the same size
		if (buf_size <= in_buf.capacity()) return false;
		ByteBuffer in_buf2 = ByteBuffer.allocateDirect(buf_size);
		ByteBuffer out_buf2 = ByteBuffer.allocateDirect(buf_size);
		in_buf.flip();
		in_buf2.put(in_buf);
		out_buf2.put(out_buf);
		out_buf2.flip();
		in_buf = in_buf2;
		out_buf = out_buf2;
		return true;
	}
	
	/**
	 * Request buffers to be resized (enlarge only).
	 * Does not resize immediately, instead resize will happen on next
	 * call to {@link #updateIn(int)} or {@link #updateOut()}.
	 * Immediate resize is dangerous because references to existing
	 * buffers become 'invalid' and bugs will ensue when done outside
	 * controlled conditions.
	 * 
	 * @param buf_size
	 * @return true iff buffers will be enlarged
	 */
	public boolean requestResizeBuffers(int buf_size) {
		// both buffers are the same size
		requested_buf_size = Math.max(requested_buf_size, buf_size);
		return requested_buf_size > in_buf.capacity();
	}
	
}
