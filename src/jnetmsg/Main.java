package jnetmsg;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;

import javax.net.ssl.SSLException;

import jnetmsg.http.HTTPClient;
import jnetmsg.http.HTTPRequest;
import jnetmsg.http.HTTPResponse;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogSink;
import jnetmsg.log.StaticLogSource;
import jnetmsg.nntp.NNTPClient;
import jnetmsg.nntp.NNTPRequest;
import jnetmsg.nntp.NNTPResponse;
import jnetmsg.nntp.NNTPSProtocol;

public class Main {

	public static final StaticLogSource LOGGER = new StaticLogSource(Main.class, "main");
	
	private static Packet testWaitPacket(Connection con) throws Exception {
		Packet p = null;
		while (p == null) {
			p = con.receive();
			Thread.sleep(10);
		}
		return p;
	}
	
	private static void testClientServer() throws Exception {
		
		ServiceDiscovery sd = Net.discover("testing", 5000);
		ConnectionListener server = Net.listen(2222, new SimplePacketProtocol());
		Net.advertise(server, "testing", "123");
		
		server.addConnectionCallback((Connection con) -> {
			if (con.status() == Connection.Status.CONNECTED) {
				con.receiveQueueBytesLimit(16384);
			}
		});
		
		Thread.sleep(2000);
		
		Connection client = Net.connect(sd.iterator().next(), new SimplePacketProtocol());
		
		for (int i = 0; i < 1; i++) {
			client.send(Packet.encodeString("i want to be the very best"));
			client.send(Packet.encodeString("like no one ever was"));
			client.send(Packet.encodeString("to catch them is my real test"));
			client.send(Packet.encodeString("to train them is my cause"));
		}
		client.shutdown();
		
		for (Packet p = null; ; p = server.receive()) {
			if (p == null) {
				Thread.sleep(50);
			} else {
				//System.out.println("server received packet: " + p.toString());
				if (p.isEOF()) {
					p.connection().shutdown();
					break;
				}
				System.out.println("server: " + p.decodeString());
				p.connection().send(Packet.encodeString(p.decodeString().toUpperCase()));
			}
		}
		
		for (Packet p = null; ; p = client.receive()) {
			if (p == null) {
				Thread.sleep(50);
			} else {
				//System.out.println("client received packet: " + p.toString());
				if (p.isEOF()) break;
				System.out.println("client: " + p.decodeString());
			}
		}
		
		Thread.sleep(2000);
		
	}
	
	private static void testHTTP() throws Exception {
		
		Connection c = Net.connect("neverssl.com", 80,  Net.protocol("http"));
		
		Packet req0 = Packet.encodeString("GET / HTTP/1.1\r\nHost: neverssl.com\r\n\r\n");
		//Packet req1 = Packet.encodeString("GET / HTTP/1.1\r\nHost: neverssl.com\r\nConnection: close\r\n\r\n");
		c.send(req0);
		c.send(req0);
		c.send(req0);
		c.shutdown();
		
		int n = 0;
		for (Packet p = null; ; p = c.receive()) {
			if (p == null) {
				Thread.sleep(50);
			} else {
				n++;
				System.out.println(p);
				if (p.isEOF()) {
					IOException e = p.connection().exception();
					if (e != null) {
						System.out.println("connection terminated: ");
						System.out.flush();
						e.printStackTrace();
					}
					break;
				}
				System.out.println(p.decodeString().substring(0, 100));
				if (n >= 3) break;
			}
		}
		
		// if we shutdown our end without sending a 'Connection: close' header,
		// it seems we can't rely on the remote host closing the connection.
		
		//c.close();
		
	}
	
	private static void testHTTPS() throws Exception {
		
		Executor async = Executors.newWorkStealingPool();
		
		URL url = new URL("https://en.wikipedia.org/wiki/Main_Page");
		
		HTTPClient client = new HTTPClient(Net.connect(url), async);
		
		HTTPRequest req = new HTTPRequest("GET", url);
		req.header("Connection", "close");
		
		client.send(req, (Result<HTTPResponse, IOException> res, HTTPClient c) -> {
			res.tryGet().ifPresent((HTTPResponse r) -> {
				LOGGER.write(LogCategory.TRACE, "received %d %s", r.code(), r.codeDescription());
				String body = r.decodeBody();
				if (body.length() >= 100) System.out.println(body.substring(0, 100));
			});
		});
		
		while (!client.isEOF()) {
			Thread.sleep(100);
		}
		
		//c.close();
		
	}
	
	private static void testNNTPS() throws Exception {
		
		Executor async = Executors.newWorkStealingPool();
		
		BufferedReader nntp_config = new BufferedReader(new FileReader("./nntp.txt"));
		String host = nntp_config.readLine();
		int port  = Integer.parseInt(nntp_config.readLine());
		String user = nntp_config.readLine();
		String pass = nntp_config.readLine();
		
		NNTPClient client0 = new NNTPClient(Net.connect(host, port, Net.protocol("nntps")), async);
		
		NNTPClient.ResponseCallback work = (Result<NNTPResponse, IOException> res0, NNTPClient c) -> {
			try {
				NNTPResponse rsp0 = res0.get();
				LOGGER.write(LogCategory.TRACE, "authinfo: %d %s", rsp0.code(), rsp0.codeDescription());
				
				if (rsp0.code() != 281) {
					LOGGER.write(LogCategory.ERROR, "authinfo failed");
					c.close();
					return;
				}
				
				c.sendMode("READER", null);
				
				c.sendCapabilities((Result<NNTPResponse, IOException> res, NNTPClient c_) -> {
					System.out.println("capabilities:");
					try {
						BufferedReader in = new BufferedReader(new InputStreamReader(new ByteBufferInputStream(res.get().body())));
						for (String line = in.readLine(); line != null; line = in.readLine()) {
							System.out.println(line);
						}
					} catch (IOException e) {
						
					}
				});
				
				c.sendGroup("comp.lang.c++", (Result<NNTPResponse, IOException> res, NNTPClient c_) -> {
					res.tryGet().ifPresent((NNTPResponse r) -> {
						if (r.code() == 211) {
							LOGGER.write(LogCategory.TRACE, "group %s: %s articles", r.argument(3), r.argument(0));
						}
					});
				});
				
				c.send(new NNTPRequest("XOVER 386430-386439"), (Result<NNTPResponse, IOException> res, NNTPClient c_) -> {
					res.tryGet().ifPresent((NNTPResponse r) -> {
						System.out.println("xover: " + r.code());
						System.out.println(r.decodeBody(StandardCharsets.UTF_8));
					});
				});
				
				c.send(new NNTPRequest("HEAD 386430"), (Result<NNTPResponse, IOException> res, NNTPClient c_) -> {
					res.tryGet().ifPresent((NNTPResponse r) -> {
						System.out.println("head: " + r.code());
						System.out.println(r.decodeBody(StandardCharsets.UTF_8));
					});
				});
				
				c.sendQuit((Result<NNTPResponse, IOException> res, NNTPClient c_) -> {
					res.tryGet().ifPresent((NNTPResponse r) -> {
						LOGGER.write(LogCategory.TRACE, "quit: %d", r.code());
					});
				});
				
			} catch (IOException e) {
				
			}
		};
		
		client0.sendAuthInfo(user, pass, work);
		
		while (!client0.isEOF()) {
			Thread.sleep(100);
		}
		
	}
	
	public static void testDotStuffing() throws Exception {
		
		Packet p0 = Packet.encodeString("100 things follow\r\n"
			+ "..thing number 1\r\n"
			+ "some other thing\r\n"
			+ "...one more thing again\r\n"
			+ "not a dot on this one\r\n"
			+ "..more dots\r\n"
			+ ".\r\n");
		
		System.out.println("original packet:");
		System.out.println(p0.decodeString());
		
		NNTPResponse r = new NNTPResponse(p0);
		System.out.println("response:");
		System.out.println(r.decodeBody(StandardCharsets.UTF_8));
		
		Packet p1 = r.toPacket();
		
		System.out.println("new packet:");
		System.out.println(p1.decodeString());
		
	}
	
	/**
	 * Simple test code.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		StaticLogSource.ROOT.attach(LogSink.STDERR);
		
		// TODO closing ssl before handshake finished may cause ssl engine internal errors etc
		// debug ssl with -Djavax.net.debug=ssl
		// upgrade jre if getting cert validation errors
		
		Net.init();
		//Net.initGatewayDiscovery();
		
		//testClientServer();
		//testHTTP();
		//testHTTPS();
		testNNTPS();
		//testDotStuffing();
		
		Net.close();
		
	}
	
}
