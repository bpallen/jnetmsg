package jnetmsg.http;

import jnetmsg.Connection;
import jnetmsg.Protocol;
import jnetmsg.ProtocolHandler;

public class HTTPProtocol implements Protocol {

	@Override
	public String name() {
		return "http";
	}

	@Override
	public ProtocolHandler newHandlerInstance(Connection con_) {
		return new HTTPProtocolHandler(con_);
	}

}
