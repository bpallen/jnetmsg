package jnetmsg.http;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import jnetmsg.Packet;
import jnetmsg.Util;

public abstract class HTTPMessage {

	// dont include CRLF
	protected String cmdline = "";
	
	// dont include CRLF
	private HashMap<String, String> headers = new HashMap<>();
	
	private ByteBuffer body = Util.EMPTY_BUFFER;
	
	protected HTTPMessage() {
		
	}
	
	protected HTTPMessage(Packet p) {
		ByteBuffer in = p.read();
		int iend = Util.findASCII(in, "\r\n\r\n");
		if (iend < 0) throw new IllegalArgumentException("malformed http message");
		cmdline = Util.getASCIIBytes(in, "\r\n");
		while (in.position() < iend) {
			String k = Util.getASCIIBytes(in, ": ");
			String v = Util.getASCIIBytes(in, "\r\n");
			if (k == null || v == null) throw new IllegalArgumentException("malformed http message");
			header(k, v);
		}
		String contentlenstr = header("Content-Length");
		if (contentlenstr != null) {
			int contentlen = Integer.parseInt(contentlenstr);
			if (contentlen < 0) throw new IllegalArgumentException("negative content-length");
			in.position(iend + 4);
			in.limit(iend + 4 + contentlen);
			body = in.slice().asReadOnlyBuffer();
		}
	}
	
	public String header(String name) {
		return headers.get(name);
	}
	
	public void header(String name, Object value) {
		Objects.requireNonNull(name);
		if (value == null) {
			headers.remove(name);
		} else {
			headers.put(name, value.toString());
		}
	}
	
	public ByteBuffer body() {
		return body.asReadOnlyBuffer();
	}
	
	public String decodeBody(Charset ch) {
		byte[] b = new byte[body.remaining()];
		body().get(b);
		return new String(b, ch);
	}
	
	public String decodeBody() {
		String h = header("Content-Type");
		if (h == null) throw new IllegalStateException("no specified content-type");
		String[] a = h.split("; ");
		for (String s : a) {
			if (s.startsWith("charset=")) {
				String ch = s.substring(8);
				return decodeBody(Charset.forName(ch));
			}
		}
		throw new IllegalStateException("no specified charset in content-type");
	}
	
	public void body(ByteBuffer b) {
		body = b == null ? Util.EMPTY_BUFFER : b.asReadOnlyBuffer();
		header("Content-Length", body.hasRemaining() ? body.remaining() : null);
	}
	
	public void encodeBody(String s, Charset ch) {
		byte[] b = s.getBytes(ch);
		body(ByteBuffer.wrap(b));
	}
	
	public int packetSize() {
		// assume ascii headers
		int sz = cmdline.length();
		sz += 2;
		for (Map.Entry<String, String> e : headers.entrySet()) {
			sz += e.getKey().length();
			sz += 2;
			sz += e.getValue().length();
			sz += 2;
		}
		sz += 2;
		sz += body.remaining();
		return sz;
	}
	
	public Packet toPacket() {
		ByteBuffer out = ByteBuffer.allocateDirect(packetSize());
		Util.putASCIIBytes(out, cmdline);
		Util.putASCIIBytes(out, "\r\n");
		for (Map.Entry<String, String> e : headers.entrySet()) {
			Util.putASCIIBytes(out, e.getKey());
			Util.putASCIIBytes(out, ": ");
			Util.putASCIIBytes(out, e.getValue());
			Util.putASCIIBytes(out, "\r\n");
		}
		Util.putASCIIBytes(out, "\r\n");
		out.put(body());
		out.flip();
		return new Packet(out);
	}
	
}
