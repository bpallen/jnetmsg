package jnetmsg.http;

import java.io.IOException;
import java.util.concurrent.Executor;

import jnetmsg.Connection;
import jnetmsg.Packet;
import jnetmsg.RequestResponseClient;
import jnetmsg.Result;

public class HTTPClient extends RequestResponseClient<HTTPRequest, HTTPResponse> {
	
	public static interface ResponseCallback extends Result.CallbackEx<HTTPResponse, IOException, HTTPClient> {}
	
	public HTTPClient(Connection con_, Executor async_) {
		super(con_, async_, null);
	}

	@Override
	protected Packet requestToPacket(HTTPRequest r) {
		return r.toPacket();
	}

	@Override
	protected HTTPResponse responseFromPacket(Packet p) {
		return new HTTPResponse(p);
	}
	
	public void send(HTTPRequest r, ResponseCallback cb) throws IOException {
		super.send(HTTPClient.class, r, cb);
	}
	
}
