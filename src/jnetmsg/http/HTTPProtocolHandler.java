package jnetmsg.http;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

import jnetmsg.Connection;
import jnetmsg.Net;
import jnetmsg.Packet;
import jnetmsg.ProtocolHandler;
import jnetmsg.SequenceNumber;
import jnetmsg.Util;
import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.StaticLogSource;

/**
 * Experimental.
 * 
 * @author Ben Allen
 *
 */
public class HTTPProtocolHandler implements ProtocolHandler {
	
	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(HTTPProtocolHandler.class, "http");
	
	private static final int INITIAL_HEADER_SIZE = 4096;
	
	private final DynamicLogSource logger;
	private final Connection con;
	private final boolean isclient;
	private final Queue<SequenceNumber> sequence = new LinkedList<>();
	
	private ByteBuffer in_head_buf = ByteBuffer.allocateDirect(INITIAL_HEADER_SIZE);
	private ByteBuffer in_body_buf;
	private ByteBuffer out_buf;
	
	private int out_seq = 0;
	
	private boolean closing = false;
	
	public HTTPProtocolHandler(Connection con_) {
		con = Objects.requireNonNull(con_);
		isclient = con.server() == null;
		logger = LOGGER.withHook(con.logger().hook());
	}
	
	@Override
	public Connection connection() {
		return con;
	}

	@Override
	public void init() {
		
	}
	
	@Override
	public boolean hasWriteInterest() {
		return hasMoreOutAppData();
	}
	
	@Override
	public boolean hasMoreOutAppData() {
		return out_buf != null;
	}

	@Override
	public ByteBuffer outBuf() {
		return out_buf;
	}

	@Override
	public ByteBuffer inBuf() {
		if (in_body_buf == null) {
			return in_head_buf;
		} else {
			return in_body_buf;
		}
	}

	@Override
	public void updateOut() throws IOException {
		if (out_buf != null && !out_buf.hasRemaining()) {
			// message complete
			out_buf = null;
		}
		if (out_buf != null) return;
		// packet preconditions are checked by checkSendable() from Connection.send()
		Packet p = collectPacket(out_seq);
		if (p == null) return;
		out_seq++;
		if (p.isEOF()) {
			out_buf = null;
			return;
		}
		out_buf = p.read();
		SequenceNumber n = p.sequenceNumber();
		if (n != null) sequence.offer(n);
	}

	@Override
	public void updateIn(int newbytes) throws IOException {
		// as any amount of messages could end up in the header buf,
		// we need to loop until we find an incomplete header or body
		while (true) {
			if (in_body_buf == null) {
				// header
				// check if header is complete
				int iheadend = Util.findASCII((ByteBuffer) in_head_buf.asReadOnlyBuffer().flip(), "\r\n\r\n");
				if (iheadend < 0) {
					// header not complete yet
					break;
				}
				// header complete
				if (iheadend >= 0) iheadend += 4;
				ByteBuffer head = in_head_buf.asReadOnlyBuffer();
				head.position(iheadend);
				head.flip();
				// find content length
				int icontentlen = Util.findASCII(head, "\r\nContent-Length: ");
				// if no content length specified, assume 0
				int contentlen = 0;
				if (icontentlen >= 0) {
					// go to field data
					icontentlen += 18;
					ByteBuffer field = head.asReadOnlyBuffer();
					field.position(icontentlen);
					int iend = Util.findASCII(field, "\r\n");
					if (iend < 0) throw new IOException("bad http content-length; no terminating CRLF");
					field.limit(iend);
					byte[] a = new byte[field.remaining()];
					field.get(a);
					try {
						contentlen = Integer.parseInt(new String(a, StandardCharsets.US_ASCII));
					} catch (NumberFormatException e) {
						throw new IOException("bad http content-length; failed to parse integer");
					}
				}
				// TODO check for unsupported transfer-encoding
				// make body buf
				in_body_buf = ByteBuffer.allocateDirect(head.limit() + contentlen);
				in_head_buf.flip();
				// can't just put the head into the body because put() doesnt allow a partial write
				Util.transfer(in_body_buf, in_head_buf);
				// prepare header buf to continue writing
				in_head_buf.compact();
			}
			if (in_body_buf != null) {
				// body
				if (in_body_buf.hasRemaining()) {
					// body not complete yet
					break;
				} else {
					// complete message, forward to application
					in_body_buf.flip();
					// TODO check against incoming sequence number values
					deliverPacket(in_body_buf, sequence.poll());
					in_body_buf = null;
				}
			}
		}
		if (!in_head_buf.hasRemaining()) {
			// need to allocate a bigger header buf
			ByteBuffer buf2 = ByteBuffer.allocateDirect((int) (in_head_buf.capacity() * 1.5));
			in_head_buf.flip();
			buf2.put(in_head_buf);
			in_head_buf = buf2;
			// need to explicitly retry because all data from net may be sitting in an outer protocol handler
			retryRead();
		}
	}
	
	@Override
	public boolean shouldCloseIn() {
		return false;
	}

	@Override
	public void closeIn() throws IOException {
		// TODO check if eof happened during message body
	}

	@Override
	public void beginCloseOut() throws IOException {
		closing = true;
	}

	@Override
	public boolean hasClosedOut() {
		// seems with http we shouldn't shutdown our end prematurely
		return closing && !hasMoreOutAppData();
	}
	
	@Override
	public boolean hasDeliveredEOF() {
		// http has no explicit eof messages
		return false;
	}

	@Override
	public void cleanup() {
		cleanupPackets();
		if (out_buf.hasRemaining()) {
			logger.write(LogCategory.WARNING, "discarded outgoing data: %d bytes", out_buf.remaining());
		}
	}

	@Override
	public void checkSendable(Packet p) {
		// nothing to do
	}

	@Override
	public SequenceNumber issueSequenceNumber(Packet p) {
		return new SequenceNumber();
	}
	
}
