package jnetmsg.http;

import java.util.Objects;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;

import jnetmsg.Connection;
import jnetmsg.Net;
import jnetmsg.Protocol;
import jnetmsg.ProtocolHandler;
import jnetmsg.SSLProtocolHandler;

public class HTTPSProtocol implements Protocol {

	private final SSLContext ssl;
	private final SSLParameters sslparams;
	private final Protocol http;
	
	public HTTPSProtocol(SSLContext ssl_, SSLParameters sslparams_, Protocol http_) {
		ssl = Objects.requireNonNull(ssl_);
		sslparams = Objects.requireNonNull(sslparams_);
		http = Objects.requireNonNull(http_);
	}
	
	@Override
	public String name() {
		return "https";
	}

	@Override
	public ProtocolHandler newHandlerInstance(Connection con_) {
		return new SSLProtocolHandler(http.newHandlerInstance(con_), ssl, sslparams);
	}

}
