package jnetmsg.http;

import jnetmsg.Packet;
import jnetmsg.SequenceNumber;

public class HTTPResponse extends HTTPMessage {

	private final SequenceNumber seq;
	
	public HTTPResponse(int code, String msg) {
		cmdline = "HTTP/1.1 " + code + " " + msg;
		seq = null;
	}
	
	public HTTPResponse(Packet p) {
		super(p);
		seq = p.sequenceNumber();
	}
	
	public SequenceNumber sequenceNumber() {
		return seq;
	}
	
	public int code() {
		return Integer.parseInt(cmdline.split(" ")[1]);
	}
	
	public String codeDescription() {
		return cmdline.split(" ")[2];
	}
	
}
