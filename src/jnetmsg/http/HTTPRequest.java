package jnetmsg.http;

import java.net.URL;

import jnetmsg.Packet;

public class HTTPRequest extends HTTPMessage {

	public HTTPRequest(String verb, URL url) {
		cmdline = verb + " " + url.getFile() + " HTTP/1.1";
		// must always set host header
		header("Host", url.getHost());
	}
	
	public HTTPRequest(Packet p) {
		super(p);
	}
	
	public String verb() {
		return cmdline.split(" ")[0];
	}
	
	public String file() {
		return cmdline.split(" ")[1];
	}
	
}
