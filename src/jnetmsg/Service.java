package jnetmsg;

import java.net.InetSocketAddress;
import java.util.Objects;

import jnetmsg.log.StaticLogSource;

/**
 * A connectable service discovered on the local network.
 * 
 * @see Net#discover(int, String, int)
 * @see Net#discover(String)
 * @see Net#connect(Service)
 * 
 * @author Ben Allen
 *
 */
public class Service {
	
	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(Service.class, "service");

	private String service_name;
	private String instance_name;
	private InetSocketAddress address;
	
	public Service(String service_name_, String instance_name_, InetSocketAddress address_) {
		service_name = Objects.requireNonNull(service_name_);
		instance_name = Objects.requireNonNull(instance_name_);
		address = Objects.requireNonNull(address_);
	}
	
	/**
	 * @return Name of this service
	 */
	public String serviceName() {
		return service_name;
	}
	
	/**
	 * @return Name of this service instance
	 */
	public String instanceName() {
		return instance_name;
	}
	
	/**
	 * @return Remote socket address of this service instance
	 */
	public InetSocketAddress remoteAddress() {
		return address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((service_name == null) ? 0 : service_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (service_name == null) {
			if (other.service_name != null)
				return false;
		} else if (!service_name.equals(other.service_name))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Service[" + remoteAddress() + ", '" + service_name + "'::'" + instance_name + "']";
	}
}
