package jnetmsg;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.LogSource;
import jnetmsg.log.StaticLogSource;

/**
 * Single thread asynchronous task execution mechanism.
 * TODO document asyncexec properly
 * 
 * @author Ben Allen
 *
 */
public class AsyncExecutor implements Closeable, Executor {

	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(AsyncExecutor.class, "async");
	
	private final DynamicLogSource logger;
	private final String name;
	private final Thread worker;
	private final BlockingQueue<Runnable> async = new LinkedBlockingQueue<>();
	private final AtomicBoolean closed = new AtomicBoolean(false);
	private volatile boolean worker_should_exit = false;
	
	/**
	 * Net must be initialized ({@link Net#init()}).
	 * 
	 * @param name_
	 */
	public AsyncExecutor(String name_) {
		name = Objects.requireNonNull(name_);
		logger = LOGGER.withHook(LogHook.appendString(" [%s]", name));
		worker = new Thread(this::run);
		worker.setDaemon(true);
		worker.setName("Net:" + name);
		worker.start();
		Net.opened(this);
	}
	
	public LogSource logger() {
		return logger;
	}
	
	public void invoke(Runnable r) {
		// note that close may happen between checking the flag and enqueue
		if (closed.get()) return;
		async.offer(r);
	}
	
	@Override
	public void execute(Runnable r) {
		invoke(r);
	}
	
	@Override
	public void close() {
		if (!closed.getAndSet(true)) {
			async.offer(() -> {
				worker_should_exit = true;
				async.clear();
			});
		}
	}

	@Override
	public String toString() {
		return "AsyncExecutor[" + name + "]";
	}
	
	private void run() {
		logger.write(LogCategory.TRACE, "async worker starting");
		while (!worker_should_exit) {
			try {
				Runnable r = async.take();
				if (r != null) r.run();
			} catch (Exception e) {
				logger.write(LogCategory.ERROR, e, "exception in %s worker");
			}
		}
		logger.write(LogCategory.TRACE, "async worker terminating");
		Net.closed(this);
	}
	
}
