package jnetmsg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.LogSource;
import jnetmsg.log.StaticLogSource;

/**
 * Generic base class for a client for request/response type protocols.
 * 
 * @author Ben Allen
 *
 */
public abstract class RequestResponseClient<Request, Response> extends AsyncTicker {
	
	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(RequestResponseClient.class, "client");

	private final Connection con;
	private final DynamicLogSource logger;
	private final List<SequenceNumber> sequence = Collections.synchronizedList(new ArrayList<>());
	
	public RequestResponseClient(Connection con_, Executor async_, LogHook loghook) {
		super(async_);
		con = Objects.requireNonNull(con_);
		logger = LOGGER.withHook(con.logger().hook()).withHook(loghook);
		con.addStatusCallback((Connection c) -> {
			if (c.status() == Connection.Status.CONNECTED) {
				onConnect();
			} else if (c.status() == Connection.Status.DISCONNECTED) {
				onDisconnect();
			}
		});
		con.addPacketWaitingCallback((Connection c) -> {
			fireTick();
		});
	}
	
	public Connection connection() {
		return con;
	}
	
	/**
	 * Use your own log source.
	 * @return
	 */
	@Deprecated
	public LogSource logger() {
		return logger;
	}
	
	public boolean isEOF() {
		return con.isEOF();
	}
	
	public int requestQueueDepth() {
		return sequence.size();
	}
	
	public void close() {
		con.close();
	}
	
	protected abstract Packet requestToPacket(Request r) throws IOException;
	
	protected abstract Response responseFromPacket(Packet p) throws IOException;
	
	protected Result<Response, IOException> newResultInstance(Packet p) {
		return new Result<Response, IOException>(() -> responseFromPacket(p));
	}
	
	protected void onEOF() {
		
	}
	
	protected void onConnect() {
		
	}
	
	protected void onDisconnect() {
		
	}
	
	protected void onUnsequenced(Result<Response, IOException> res) {
		
	}
	
	protected void onException(IOException e) {
		
	}
	
	protected <C extends RequestResponseClient<Request, Response>> void send(Class<C> ctxcls, Request r, Result.CallbackEx<Response, IOException, C> cb) throws IOException {
		// check we can pass this to the callback
		if (!ctxcls.isInstance(this)) throw new AssertionError("bad callback context type");
		synchronized (sequence) {
			// sync to avoid potential race where response received before setting callback
			SequenceNumber n = con.send(requestToPacket(r));
			if (n == null && cb != null) throw new AssertionError("callback not possible");
			if (n != null) n.user = cb;
			sequence.add(n);
		}
	}
	
	@Override
	protected void tick(int ticks) {
		try {
			for (Packet p = con.receive(); p != null; p = con.receive()) {
				if (p.isEOF()) {
					cleanup(new NoResponseException("connection eof"));
					onEOF();
					return;
				} else {
					Result<Response, IOException> res = newResultInstance(p);
					SequenceNumber n = p.sequenceNumber();
					if (n == null) {
						onUnsequenced(res);
					} else {
						sequence.remove(n);
						execSeqCallback(n, res);
					}
				}
			}
		} catch (IOException e) {
			cleanup(e);
			onException(e);
		}
	}
	
	private void execSeqCallback(SequenceNumber n, Result<Response, IOException> res) {
		// should be mostly fine, see class check in send()
		@SuppressWarnings("unchecked")
		Result.CallbackEx<Response, IOException, RequestResponseClient<Request, Response>> cb = (Result.CallbackEx<Response, IOException, RequestResponseClient<Request, Response>>) n.user;
		if (cb != null) {
			try {
				cb.execute(res, this);
			} catch (RuntimeException e) {
				logger.write(LogCategory.ERROR, e, "exception in client result callback");
			}
		}
	}
	
	private void cleanup(IOException e) {
		synchronized (sequence) {
			for (SequenceNumber n : sequence) {
				// deliver error to all pending response callbacks
				execSeqCallback(n, new Result<Response, IOException>(e));
			}
			sequence.clear();
		}
	}
	
}
