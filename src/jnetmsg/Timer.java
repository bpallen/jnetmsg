package jnetmsg;

import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import jnetmsg.log.LogCategory;
import jnetmsg.log.StaticLogSource;

/**
 * TODO document timer properly
 * 
 * @author Ben Allen
 *
 */
public class Timer {
	
	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(Timer.class, "timer");

	public static interface TimerCallback {
		/**
		 * @return true iff the timer should be queued again
		 */
		public boolean execute(long elapsed_millis);
	}
	
	private static class DelayedInvocation implements Comparable<DelayedInvocation> {
		public long start;
		public long delay;
		public long expiry;
		public TimerCallback cb;
		
		@Override
		public int compareTo(DelayedInvocation arg0) {
			return Long.compare(expiry, arg0.expiry);
		}
	}
	
	private static final Thread worker = new Thread(Timer::run);
	private static final AtomicBoolean closed = new AtomicBoolean(false);
	private static volatile boolean worker_should_exit = false;
	private static final BlockingQueue<Runnable> async = new LinkedBlockingQueue<>();
	private static final PriorityQueue<DelayedInvocation> queue = new PriorityQueue<DelayedInvocation>();
	private static final Semaphore wakeup = new Semaphore(0);
	
	static {
		worker.setDaemon(true);
		worker.setName("NetTimers");
		worker.start();
	}
	
	public static void invoke(Runnable r) {
		// note that shutdown may happen between checking the flag and enqueue 
		if (closed.get()) return;
		async.offer(r);
		wakeup.release();
	}
	
	public static void add(long delay_millis, boolean immediate, TimerCallback cb) {
		if (closed.get()) return;
		long now = System.currentTimeMillis();
		long expiry = now + (immediate ? 0 : delay_millis);
		invoke(() -> {
			DelayedInvocation d = new DelayedInvocation();
			d.start = now;
			d.delay = delay_millis;
			d.expiry = expiry;
			d.cb = cb;
			queue.offer(d);
		});
	}
	
	static void close() {
		if (!closed.getAndSet(true)) {
			async.offer(() -> {
				worker_should_exit = true;
				async.clear();
			});
			wakeup.release();
			// wait for worker exit
			while (worker.isAlive()) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) { }
			}
		}
	}
	
	private static void run() {
		LOGGER.write(LogCategory.TRACE, "worker starting");
		while (true) {
			try {
				// execute pending tasks
				Runnable r = null;
				while ((r = async.poll()) != null) r.run();
				if (worker_should_exit) break;
				// execute expired timers
				DelayedInvocation d = null;
				long now = System.currentTimeMillis();
				for (d = queue.peek(); d != null && d.expiry <= now; d = queue.peek()) {
					queue.poll();
					if (d.cb.execute(now - d.start)) {
						d.expiry += d.delay;
						queue.offer(d);
					}
				}
				// sleep until the next timer expires or we get interrupted
				long expiry = d == null ? now + 5000 : d.expiry;
				try {
					wakeup.tryAcquire(expiry - now, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) { }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		LOGGER.write(LogCategory.TRACE, "worker terminating");
	}
	
}
