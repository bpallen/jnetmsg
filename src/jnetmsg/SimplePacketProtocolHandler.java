package jnetmsg;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * Simple packet protocol.
 * TODO document this properly
 * 
 * @author Ben Allen
 *
 */
public class SimplePacketProtocolHandler implements ProtocolHandler {
	
	public static final int MAX_PACKET_SIZE = 65534;
	
	private static final int HEADER_SIZE = 2;
	
	private final Connection con;
	private boolean closing = false;
	private boolean read_eof = false;
	private boolean write_eof = false;
	
	// incoming packet
	private final ByteBuffer in_head_buf = ByteBuffer.allocateDirect(HEADER_SIZE);
	private ByteBuffer in_body_buf = null;
	
	// outgoing packet
	private final ByteBuffer out_head_buf = ByteBuffer.allocateDirect(HEADER_SIZE);
	private ByteBuffer out_body_buf = null;
	
	public SimplePacketProtocolHandler(Connection con_) {
		con = Objects.requireNonNull(con_);
		// need to set the out header buf to 'fully written'
		out_head_buf.position(out_head_buf.limit());
	}
	
	private void maybeClose() {
		if (read_eof && write_eof) {
			connection().close();
		}
	}
	
	@Override
	public Connection connection() {
		return con;
	}
	
	@Override
	public void init() {
		
	}

	@Override
	public boolean hasWriteInterest() {
		return hasMoreOutAppData();
	}
	
	@Override
	public boolean hasMoreOutAppData() {
		return out_head_buf.hasRemaining() || out_body_buf != null;
	}

	@Override
	public ByteBuffer outBuf() {
		if (out_head_buf.hasRemaining()) {
			// header
			return out_head_buf;
		} else {
			// body (maybe null)
			return out_body_buf;
		}
	}

	@Override
	public ByteBuffer inBuf() {
		if (in_head_buf.hasRemaining()) {
			// header
			return in_head_buf;
		} else {
			// body
			return Objects.requireNonNull(in_body_buf);
		}
	}

	@Override
	public void updateOut() throws IOException {
		if (out_body_buf != null && !out_body_buf.hasRemaining()) {
			// body complete
			out_body_buf = null;
		}
		if (!out_head_buf.hasRemaining() && out_body_buf == null) {
			// begin new header and body
			// packet preconditions are checked by checkSendable() from Connection.send()
			Packet p = collectPacket(-1);
			// no more packets
			if (p == null) return;
			if (p.isEOF()) write_eof = true;
			// switch header buf to writing
			out_head_buf.clear();
			// prepare header
			out_head_buf.putShort((short) (p.isEOF() ? 0xFFFF : p.size()));
			// switch header buf back to reading
			out_head_buf.flip();
			// prepare body
			out_body_buf = p.isEOF() ? null : p.read();
			maybeClose();
		}
	}

	@Override
	public void updateIn(int newbytes) throws IOException {
		if (in_body_buf == null) {
			// header
			if (!in_head_buf.hasRemaining()) {
				// complete header, switch buf to reading
				in_head_buf.flip();
				int size = 0xFFFF & (int) in_head_buf.getShort();
				if (size == 0xFFFF) {
					// explicit eof from remote
					read_eof = true;
					// switch header buf back to writing
					in_head_buf.clear();
					// deliver 'clean' eof packet to application
					deliverEOF();
					maybeClose();
				} else {
					// allocate buffer for body
					in_body_buf = ByteBuffer.allocateDirect(size);
					// note: entire header buf must be consumed here and not cleared
				}
			}
		} else {
			// body
			if (!in_body_buf.hasRemaining()) {
				// complete packet, forward to application
				in_body_buf.flip();
				deliverPacket(in_body_buf, null);
				in_body_buf = null;
				// switch header buf back to writing
				in_head_buf.clear();
			}
		}
	}
	
	@Override
	public boolean shouldCloseIn() {
		return read_eof;
	}

	@Override
	public void closeIn() throws IOException {
		if (in_body_buf != null) {
			// reached EOF while reading packet body, dirty finish
			throw new IOException("SimplePacketProtocol: reached EOF during packet body");
		}
	}

	@Override
	public void beginCloseOut() throws IOException {
		closing = true;
	}

	@Override
	public boolean hasClosedOut() {
		return closing && !hasMoreOutAppData();
	}
	
	@Override
	public boolean hasDeliveredEOF() {
		return read_eof;
	}

	@Override
	public void cleanup() {
		cleanupPackets();
	}

	@Override
	public void checkSendable(Packet p) {
		if (p.size() > MAX_PACKET_SIZE) throw new IllegalArgumentException("Packet is too big (" + p.size() + ")");
	}

	@Override
	public SequenceNumber issueSequenceNumber(Packet p) {
		// no sequence numering possible
		return null;
	}

}
