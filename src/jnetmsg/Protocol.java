package jnetmsg;

public interface Protocol {

	public String name();
	
	public ProtocolHandler newHandlerInstance(Connection con_);
	
}
