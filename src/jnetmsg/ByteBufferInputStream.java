package jnetmsg;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Objects;

public class ByteBufferInputStream extends InputStream {

	private final ByteBuffer buf;
	
	public ByteBufferInputStream(ByteBuffer buf_) {
		buf = Objects.requireNonNull(buf_);
	}
	
	@Override
	public int read() {
		if (!buf.hasRemaining()) return -1;
		return buf.get() & 0xFF;
	}

	@Override
	public int read(byte[] b, int off, int len) {
		if (!buf.hasRemaining()) return -1;
		len = Math.min(len, buf.remaining());
        buf.get(b, off, len);
        return len;
	}

	@Override
	public int available() {
		return buf.remaining();
	}

	@Override
	public synchronized void mark(int readlimit) {
		buf.mark();
	}

	@Override
	public synchronized void reset() {
		buf.reset();
	}

	@Override
	public boolean markSupported() {
		return true;
	}

}
