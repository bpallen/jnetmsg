package jnetmsg;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.LogSource;
import jnetmsg.log.StaticLogSource;

/**
 * A connection to a remote host.
 * 
 * @see Net#connect(InetSocketAddress)
 * @see Net#connect(String, int)
 * 
 * @author Ben Allen
 *
 */
public class Connection implements Closeable {

	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(Connection.class, "con");
	
	// this is the key attachment
	private class ReadWriteable extends Net.ReadWriteable {
		@Override
		void read() {
			Connection.this.read();
		}

		@Override
		void write() {
			Connection.this.write();
		}
		
		@Override
		void connect() {
			Connection.this.connect();
		}
		
		Connection connection() {
			return Connection.this;
		}
	}
	
	public static final int CLOSE_INACTIVITY_TIMEOUT = 10000;
	
	/**
	 * Default value for {@link Connection#receiveQueueBytesLimit()}.
	 */
	public static final int DEFAULT_RECEIVE_QUEUE_LIMIT_BYTES = 512 * 1024;
	
	public static enum Status {
		CONNECTING, CONNECTED, DISCONNECTING, DISCONNECTED
	}
	
	public static interface StatusCallback {
		public void execute(Connection h);
	}

	private final SelectionKey key;
	private final InetSocketAddress intended_remote;
	private final ConnectionListener server;
	private final ProtocolHandler proto;
	
	private volatile Status status = Status.CONNECTING;
	
	// only used from net thread
	private final Set<StatusCallback> status_callbacks = new HashSet<>();
	private final Set<StatusCallback> packet_callbacks = new HashSet<>();
	
	// pending packets (package visibility; used by protocol handlers)
	final Queue<Packet> incoming_packets = new LinkedBlockingQueue<>();
	final Queue<Packet> outgoing_packets = new LinkedBlockingQueue<>();
	
	// outgoing application bytes (estimate): for rate limiting; also resolves send/shutdown race
	final AtomicInteger outgoing_bytes = new AtomicInteger(0);
	
	// incoming application bytes (estimate): for rate limiting
	final AtomicInteger incoming_bytes = new AtomicInteger(0);
	
	// max value for incoming_bytes before triggering rate limiting
	private volatile int incoming_bytes_limit = DEFAULT_RECEIVE_QUEUE_LIMIT_BYTES; 
	
	// cached addresses
	private volatile InetSocketAddress local_address;
	private volatile InetSocketAddress remote_address;
	
	// has sending from application finished?
	private final AtomicBoolean shutdown = new AtomicBoolean(false);
	
	// has the connection been closed by the application?
	private final AtomicBoolean closed = new AtomicBoolean(false);
	
	// have we begun closing the protocol handler? (net thread only)
	private boolean proto_closing = false;
	
	// have we sent an eof packet? (net thread only)
	private boolean sent_eof = false;
	
	// has sending to net finished?
	private volatile boolean write_eof = false;
	
	// has reading from net finished?
	private volatile boolean read_eof = false;
	
	// time of last read operation
	private volatile long time_last_read = System.currentTimeMillis();
	
	// time of last write operation
	private volatile long time_last_write = time_last_read;
	
	// time that close was initiated
	private volatile long time_close = time_last_read;
	
	// exception that caused connection termination, if any
	private volatile IOException exception = null;
	
	private final DynamicLogSource logger = LOGGER.withHook(LogHook.appendDynamicString(" [%s]", this::description));
	
	Connection(SelectionKey key_, InetSocketAddress remote_, Protocol proto_, ConnectionListener server_) throws IOException {
		key = Objects.requireNonNull(key_);
		intended_remote = Objects.requireNonNull(remote_);
		server = server_;
		proto = Objects.requireNonNull(proto_.newHandlerInstance(this));
		if (channel().isBlocking()) throw new IOException("channel in blocking mode");
		// note: already-connected channels are produced by server accept 
		if (channel().isConnected()) status = Status.CONNECTED;
		if (!channel().isOpen()) status = Status.DISCONNECTED;
		key.attach(new ReadWriteable());
		if (status != Status.DISCONNECTED) Net.opened(this);
		// note: there can't possibly be any status callbacks yet
	}
	
	private void execCallback(StatusCallback cb) {
		try {
			cb.execute(this);
		} catch (RuntimeException e) {
			logger.write(LogCategory.ERROR, e, "exception in status callback");
		}
	}
	
	SelectionKey key() {
		return key;
	}
	
	public ConnectionListener server() {
		return server;
	}
	
	SocketChannel channel() {
		return (SocketChannel) key.channel();
	}
	
	public String description() {
		return (local_address == null ? "???" : local_address.toString()) + " <-> "
			+ (remote_address == null ? intended_remote.toString() : remote_address.toString());
	}
	
	public LogSource logger() {
		return logger;
	}
	
	/**
	 * @return Local socket address of this connection or null if not yet determined
	 */
	public InetSocketAddress localAddress() {
		return local_address;
	}
	
	/**
	 * @return Remote socket address of this connection or null if not yet determined
	 */
	public InetSocketAddress remoteAddress() {
		return remote_address;
	}
	
	/**
	 * @return Remote socket address to which this connection was initiated or accepted from
	 */
	public InetSocketAddress intendedRemoteAddress() {
		return intended_remote;
	}
	
	/**
	 * @return Status of this connection.
	 */
	public Status status() {
		return status;
	}

	/**
	 * @return Value of {@link System#currentTimeMillis()} when data (including EOF) was
	 * last read from the network, or when the connection was constructed if no data has
	 * been read
	 */
	public long timeLastRead() {
		return time_last_read;
	}
	
	/**
	 * @return Value of {@link System#currentTimeMillis()} when data (including EOF) was
	 * last written to the network, or when the connection was constructed if no data has
	 * been written
	 */
	public long timeLastWrite() {
		return time_last_write;
	}
	
	/**
	 * @return Max of {@link #timeLastRead()} and {@link #timeLastWrite()}
	 */
	public long timeLastActivity() {
		return Math.max(time_last_read, time_last_write);
	}
	
	/**
	 * Get the max value of {@link #receiveQueueBytesEstimate()} before network rate
	 * limiting is triggered. Default value is {@link #DEFAULT_RECEIVE_QUEUE_LIMIT_BYTES}
	 * ({@value #DEFAULT_RECEIVE_QUEUE_LIMIT_BYTES}).
	 * 
	 * @return Receive queue limit
	 */
	public int receiveQueueBytesLimit() {
		return incoming_bytes_limit;
	}
	
	/**
	 * Set {@link #receiveQueueBytesLimit()}.
	 * 
	 * @param limit
	 */
	public void receiveQueueBytesLimit(int limit) {
		incoming_bytes_limit = limit;
	}
	
	/**
	 * Get an estimate of the number of bytes of application data waiting to be received
	 * ({@link #receive()}). If this is greater than {@link #receiveQueueBytesLimit()}
	 * network rate limiting will be triggered.
	 * 
	 * @return Byte count estimate
	 */
	public int receiveQueueBytesEstimate() {
		return incoming_bytes.get();
	}
	
	/**
	 * Get an estimate of the number of bytes of application data waiting
	 * to be written to the network, having already been sent ({@link #send(Packet)}).
	 * Applications should use this to implement rate limiting for sending if required.
	 * 
	 * @return Byte count estimate
	 */
	public int sendQueueBytesEstimate() {
		return outgoing_bytes.get();
	}
	
	/**
	 * Add a status callback to this connection. The callback will be called
	 * whenever the status of the connection is changed.
	 * 
	 * If the status when the callback is added is not {@link Status#CONNECTING},
	 * the callback is called as soon as possible.
	 * 
	 * Status callbacks are always called from the Net worker thread.
	 * 
	 * @param cb Callback to add
	 */
	public void addStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			if (status_callbacks.add(cb) && status != Status.CONNECTING) {
				execCallback(cb);
			}
		});
	}
	
	/**
	 * Remove a status callback.
	 * 
	 * @param cb Callback to remove
	 */
	public void removeStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			status_callbacks.remove(cb);
		});
	}
	
	/**
	 * Add a packet waiting callback to this connection. The callback will be called
	 * whenever a new {@link Packet} is queued for {@link #receive()} and is then
	 * observed to be the only packet in the queue. There is no guarantee that there
	 * will actually be a packet to receive when the callback is called.
	 * 
	 * This is intended to be used to trigger a processing task on a separate thread
	 * that then calls {@link #receive()}. <b>DO NOT</b> attempt to receive packets
	 * from the callback, as this defeats the point of the receive queue. Receiving
	 * on the net thread would mean that either the net thread blocks until processing
	 * is complete (bad) or that the received packet has to be put in some other form
	 * of queue, which is not only pointless but defeats the network rate limiting
	 * (also bad) ({@link #receiveQueueBytesLimit()}).
	 * 
	 * If the queue is not empty when the callback is added, the callback is called
	 * as soon as possible.
	 * 
	 * Packet waiting callbacks are always called from the Net worker thread.
	 * 
	 * @param cb Callback to add
	 */
	public void addPacketWaitingCallback(StatusCallback cb) {
		Net.invoke(() -> {
			if (packet_callbacks.add(cb) && incoming_bytes.get() > 0) {
				execCallback(cb);
			}
		});
	}
	
	/**
	 * Remove a packet waiting callback.
	 * 
	 * @param cb Callback to remove
	 */
	public void removePacketWaitingCallback(StatusCallback cb) {
		Net.invoke(() -> {
			packet_callbacks.remove(cb);
		});
	}
	
	/**
	 * Receive a packet from the remote host at the other end of this connection.
	 * If no complete packets are waiting to be received, returns null.
	 * When the remote host has closed the connection, returns a <i>single</i>
	 * packet where {@link Packet#isEOF()} returns <code>true</code>, after which
	 * null is returned indefinitely. If the packet has a {@link Packet#sequenceNumber()},
	 * it will be the same object (i.e. <code>==</code>) as one earlier returned by
	 * {@link #send(Packet)}, indicating that this packet is the matching response.
	 * This method will only throw if all waiting packets have been received.
	 * Upon throwing, no EOF packet will be returned, and all subsequent invocations
	 * will also throw. Will not throw if the connection has closed normally.
	 * 
	 * @return Received {@link Packet} or null
	 * @throws IOException if the connection terminated abnormally
	 */
	public Packet receive() throws IOException {
		Packet p = incoming_packets.poll();
		if (p == null) {
			if (exception != null) {
				throw new IOException("connection terminated abnormally", exception);
			}
			return null;
		} else {
			final int l = incoming_bytes_limit;
			final int q = queueSize(p);
			final int c = incoming_bytes.addAndGet(-q);
			if (c <= l && c + q > l) {
				// rate limiting was in effect but need be no longer; remove it
				// this is queued to the net thread, so will always happen after
				// the net thread has actually removed read interest
				retryRead();
			}
			return p;
		}
	}
	
	void deliverPacketImpl(ByteBuffer buf, SequenceNumber seq) {
		if (seq != null) seq.time_rcvd = Instant.now();
		// null buf -> eof packet
		Packet p = new Packet(buf, this, seq);
		final int l = receiveQueueBytesLimit();
		final int q = queueSize(p);
		final int c = incoming_bytes.addAndGet(q);
		if (c > l && c - q <= l) {
			// receive queue limit exceeded, introduce rate limiting
			uninterestReadImmediate();
		}
		incoming_packets.add(p);
		final int b = incoming_bytes.get();
		// b < q => b == 0 => nothing to do
		// b > q => multiple packets in queue, receiver should be already awake
		// b == q => only packet in queue, fire callbacks
		if (b == q) {
			for (StatusCallback cb : packet_callbacks) {
				execCallback(cb);
			}
		}
	}
	
	void interestWrite() {
		// add write to interest ops in a threadsafe manner
		Net.invoke(this::interestWriteImmediate);
	}
	
	void interestWriteImmediate() {
		key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
	}
	
	void uninterestWriteImmediate() {
		key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
	}
	
	void uninterestReadImmediate() {
		key.interestOps(key.interestOps() & ~SelectionKey.OP_READ);
	}
	
	void retryRead() {
		Net.invoke(() -> {
			// ensure read is in interest ops
			key.interestOps(key.interestOps() | SelectionKey.OP_READ);
			// actually attempt to read to push stagnant data towards the application
			read();
		});
	}
	
	int queueSize(Packet p) {
		return Math.max(1, p.size());
	}
	
	/**
	 * Send a packet to the remote host at the other end of this connection.
	 * Does not wait for sending to complete. Applications should use
	 * {@link #sendQueueBytesEstimate()} to implement rate limiting if required.
	 * {@link Packet#connection()} and {@link Packet#sequenceNumber()} will be ignored.
	 * Returns the {@link SequenceNumber} for this packet if issued one by the protocol.
	 * The sequence number's value will <i>not</i> be set immediately.
	 * 
	 * @param p {@link Packet} to send
	 * @return {@link SequenceNumber} for this packet, or null
	 * @throws IllegalArgumentException if the packet {@link Packet#isEOF()} or the protocol cannot send it
	 * @throws IOException if the connection has been shutdown or has terminated abnormally
	 */
	public SequenceNumber send(Packet p) throws IOException {
		Objects.requireNonNull(p);
		// check for eof (not valid, does not 'send eof')
		if (p.isEOF()) throw new IllegalArgumentException("cannot explicitly send eof");
		proto.checkSendable(p);
		// construct new packet with sequence number
		p = new Packet(p.buf, this, proto.issueSequenceNumber(p));
		// add before testing shutdown
		// the reason outgoing_bytes is an 'estimate' is that we must add at least 1
		// for each sent packet, but 0-size packets are perfectly valid
		outgoing_bytes.addAndGet(queueSize(p));
		if (shutdown.get()) {
			outgoing_bytes.addAndGet(-queueSize(p));
			// still need to try write even if abandoning send because we may have prevented
			// shutdown from starting due to incrementing the outgoing byte counter 
			interestWrite();
			if (exception == null) {
				throw new IOException("connection has been shutdown");
			} else {
				throw new IOException("connection terminated abnormally", exception);
			}
		}
		outgoing_packets.offer(p);
		interestWrite();
		return p.sequenceNumber();
	}
	
	Packet collectPacketImpl(int seq) {
		Packet p = outgoing_packets.poll();
		if (p != null) {
			outgoing_bytes.addAndGet(-queueSize(p));
			if (p.seq != null) {
				p.seq.value = seq;
				p.seq.time_sent = Instant.now();
			}
		}
		return p;
	}
	
	/**
	 * Shutdown this connection. No further sending will be allowed, but packets will still
	 * be received until the connection is closed.
	 * 
	 * Does not wait for sending to complete.
	 * 
	 */
	public void shutdown() {
		if (!shutdown.getAndSet(true)) {
			// ensure something happens from net thread
			interestWrite();
		}
	}
	
	/**
	 * Close this connection. First calls {@link #shutdown()}.
	 * After all outgoing packets have been written to the network and at least 500ms
	 * has elapsed since the last network read, protocol closure will be initiated.
	 * The underlying network connection will then be shutdown, and any remaining
	 * data will be read from the network.
	 * 
	 * If {@link #CLOSE_INACTIVITY_TIMEOUT} ms elapses without
	 * any network activity, {@link #terminate(String)} will be called.
	 */
	@Override
	public void close() {
		if (!closed.getAndSet(true)) {
			time_close = System.currentTimeMillis();
			shutdown();
			// ensure something happens from net thread
			// necessary here too in case this wasn't the first call to shutdown()
			interestWrite();
			// start close inactivity timer
			Timer.add(1000, false, (long elapsed) -> {
				return checkCloseTimeout();
			});
		}
	}
	
	/**
	 * Called during {@link Net#close()} to help ensure closure happens in a timely manner,
	 * especially as {@link Timer} is not available at that time.
	 */
	@Override
	public void expediteClosure() {
		// ensure write() actually sees the read timeout
		interestWrite();
		// ensure close inactivity timeout still happens
		checkCloseTimeout();
	}
	
	/**
	 * Terminate the network connection immediately, discarding sent data as necessary.
	 * 
	 * @param reason Termination explanation for logging
	 */
	public void terminate(String reason) {
		Net.invoke(() -> {
			logger.write(LogCategory.WARNING, "forcing termination: %s", reason);
			SocketChannel ch = channel();
			try {
				// prevent further writes
				write_eof = true;
				uninterestWriteImmediate();
				// ensure we read eof (e.g. ssl protocol can remove read interest during handshake)
				retryRead();
				try {
					// shutdown input so we can still read eof
					ch.shutdownInput();
					// shutdown output so we send immediate eof
					ch.shutdownOutput();
				} catch (NotYetConnectedException e) {
					// force connect to fail
					ch.close();
					connect();
				}
			} catch (ClosedChannelException e) {
				// nothing to do
				return;
			} catch (IOException e) {
				logger.write(LogCategory.ERROR, "error terminating: %s (%s)", e.getMessage(), e.getClass().getSimpleName());
				exception = e;
				cleanup();
			}
		});
	}
	
	/**
	 * Returns true if the underlying network connection has been read to EOF
	 * and all waiting packets have been received by the application.
	 * 
	 * Will only return true after the EOF packet has been received
	 * (if one is produced; {@link #receive()} may throw instead).
	 * 
	 * @see Packet#isEOF()
	 * 
	 * @return True if connection is at EOF
	 */
	public boolean isEOF() {
		return read_eof && incoming_packets.isEmpty();
	}
	
	/**
	 * @return Exception that caused connection termination, or null
	 */
	public IOException exception() {
		return exception;
	}
	
	private void status(Status s) {
		Status old = status;
		status = s;
		if (s != old) {
			for (StatusCallback cb : status_callbacks) {
				execCallback(cb);
			}
		}
	}
	
	private void cleanup() {
		status(Status.DISCONNECTING);
		if (channel().isConnected()) {
			logger.write(LogCategory.TRACE, "disconnect");
		}
		try {
			key.cancel();
			channel().close();
		} catch (Exception e2) { }
		try {
			proto.cleanup();
		} catch (Exception e3) { }
		// should not deliver eof packet after error
		// so don't need to do it here
		read_eof = true;
		write_eof = true;
		shutdown.set(true);
		closed.set(true);
		Net.closed(this);
		status(Status.DISCONNECTED);
		// fire packet waiting callbacks so anything waiting can wake up
		for (StatusCallback cb : packet_callbacks) {
			execCallback(cb);
		}
	}
	
	private boolean checkCloseTimeout() {
		final long now = System.currentTimeMillis();
		if (read_eof && write_eof) {
			// disconnected normally 
			return false;
		}
		if (Math.max(timeLastActivity(), time_close) + CLOSE_INACTIVITY_TIMEOUT < now) {
			// application has closed connection and no activity for X seconds
			terminate("close inactivity timeout");
			return false;
		}
		// still waiting
		return true;
	}
	
	void connect() {
		SocketChannel ch = channel();
		try {
			if (ch.finishConnect()) {
				local_address = (InetSocketAddress) channel().getLocalAddress();
				// TODO is the actual remote address necessarily the same as the one we connected to?
				remote_address = (InetSocketAddress) channel().getRemoteAddress();
				logger.write(LogCategory.TRACE, "connect");
				// remove connect interest, add read interest (write may be already set)
				key.interestOps((key.interestOps() & ~SelectionKey.OP_CONNECT) | SelectionKey.OP_READ);
				// signal protocol handler that the channel is connected
				proto.init();
				status(Status.CONNECTED);
			}
		} catch (IOException e) {
			// TODO intended local/remote address how?
			logger.write(LogCategory.ERROR, "error connecting: %s (%s)", e.getMessage(), e.getClass().getSimpleName());
			exception = e;
			cleanup();
		}
	}
	
	void read() {
		if (read_eof) return;
		SocketChannel ch = channel();
		try {
			int c = 0;
			do {
				final long now = System.currentTimeMillis();
				ByteBuffer buf = proto.inBuf();
				c = buf == null ? 0 : ch.read(buf);
				//System.err.println("read " + c);
				// update timestamp only if data was actually read
				if (c > 0) time_last_read = now;
				boolean has_read_interest = proto.hasReadInterest();
				proto.updateIn(Math.max(c, 0));
				if (proto.shouldCloseIn()) ch.shutdownInput();
				if (c < 0 && !read_eof) {
					// eof reached (may throw if protocol detects 'dirty' eof)
					proto.closeIn();
					// forward 'clean' eof packet to application if needed
					if (!proto.hasDeliveredEOF()) proto.deliverEOF();
					uninterestReadImmediate();
					// set read_eof after forwarding packet to avoid isEOF() returning true prematurely
					read_eof = true;
					time_last_read = now;
					ch.shutdownInput();
					if (write_eof) cleanup();
					return;
				}
				if (!has_read_interest) {
					// unable to read immediately, remove read interest
					uninterestReadImmediate();
				}
			} while (c > 0);
		} catch (IOException e) {
			logger.write(LogCategory.ERROR, "error reading: %s (%s)", e.getMessage(), e.getClass().getSimpleName());
			exception = e;
			cleanup();
		}
	}
	
	void write() {
		// don't try to write after clean channel shutdown
		if (write_eof) return;
		SocketChannel ch = channel();
		try {
			int c = 0;
			do {
				final long now = System.currentTimeMillis();
				// must observe these flags before observing 'no more data' to prevent races
				final boolean was_shutdown = shutdown.get();
				final boolean was_closed = closed.get();
				proto.updateOut();
				// must check this before consuming data from buffer
				// something like ssl may have no write interest during handshake without consuming all app data
				boolean has_write_interest = proto.hasWriteInterest();
				ByteBuffer buf = proto.outBuf();
				// always try to write so we get an exception if output is shutdown
				c = ch.write(buf == null ? Util.EMPTY_BUFFER : buf);
				// update timestamp only if data was actually written
				if (c > 0) time_last_write = now;
				//System.err.println("write " + c);
				// TODO what if the connection is idle, hasMore never returns false, but we want to close?
				if (!proto.hasMoreOutAppData() && outgoing_bytes.get() == 0) {
					// all available packets from application have been written
					if (was_shutdown && !write_eof) {
						// application will send no more packets
						if (!sent_eof) {
							// synthesize eof packet so it can be collected by the protocol handler
							Packet p = new Packet(null, this, null);
							outgoing_bytes.addAndGet(queueSize(p));
							outgoing_packets.offer(p);
							sent_eof = true;
							// retry write instead of proceeding under now-invalid assumptions
							continue;
						}
						if (proto.hasClosedOut()) {
							// protocol says we can shutdown channel
							// sending finished, clean
							uninterestWriteImmediate();
							write_eof = true;
							time_last_write = now;
							ch.shutdownOutput();
							if (read_eof) cleanup();
							return;
						} else if (was_closed && !proto_closing) {
							final long dur_until_close = 500 - (now - time_last_read);
							if (dur_until_close <= 0) {
								// no more application packets, but protocol may still have work to do
								proto_closing = true;
								proto.beginCloseOut();
								// reinstate write interest so closure can continue
								has_write_interest = true;
							} else {
								// wait for read timeout before closing
								// if shutting down, the timer won't happen but expediteClosure() will
								Timer.add(dur_until_close + 20, false, (long elapsed_millis) -> {
									interestWrite();
									return false;
								});
							}
						}
					}
				}
				if (!has_write_interest) {
					// no new data to write immediately, remove write interest
					uninterestWriteImmediate();
					return;
				}
			} while (c > 0);
		} catch (IOException e) {
			logger.write(LogCategory.ERROR, "error writing: %s (%s)", e.getMessage(), e.getClass().getSimpleName());
			exception = e;
			cleanup();
		}
	}

	@Override
	public int hashCode() {
		return key.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (o.getClass() != getClass()) return false;
		return key.equals(((Connection) o).key);
	}
	
	@Override
	public String toString() {
		return "Connection[" + description() + "]";
	}
}
