package jnetmsg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.StaticLogSource;

public class Gateway implements Closeable {
	
	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(Gateway.class, "gateway");

	// UPnP SOAP command reference:
	// http://upnp.org/specs/gw/UPnP-gw-WANIPConnection-v1-Service.pdf
	
	static class ControlException extends IOException {
		
		private static final long serialVersionUID = 1L;
		private int upnperror;
		
		public ControlException(int upnperror_, String msg_) {
			super(msg_ +  " (UPnP " + upnperror_ + ")");
			upnperror = upnperror_;
		}
		
		public int getUPnPError() {
			return upnperror;
		}
	}

	private static AsyncExecutor gateway_async;
	
	static synchronized void staticInit() {
		if (gateway_async == null) gateway_async = new AsyncExecutor("gateway");
	}
	
	// delay between port status queries in millis
	private static final int PORT_DELAY = 5000;
	
	// usable services
	private static final String[] services = new String[] {
		"urn:schemas-upnp-org:service:WANIPConnection:1",
        "urn:schemas-upnp-org:service:WANPPPConnection:1"
	};
	
	private final DynamicLogSource logger = LOGGER.withHook(LogHook.appendDynamicString(" [%s]", this::address));
	
	private final InetAddress address;
	private final InetAddress forward_address;
	private String location;
	private final Map<Integer, PortMapping.Status> port_status_tcp = Collections.synchronizedMap(new HashMap<>());
	private final Map<Integer, PortMapping.Status> port_status_udp = Collections.synchronizedMap(new HashMap<>());
	
	private volatile InetAddress external_address;
	private final AtomicBoolean closed = new AtomicBoolean(false);
	
	// only used by gateway thread
	private String service;
	private String controlurl;
	private Set<Integer> pending_ports = new HashSet<>();
	
	Gateway(InetAddress address_, InetAddress forward_address_) {
		address = address_;
		forward_address = forward_address_;
	}
	
	public InetAddress address() {
		return address;
	}
	
	public InetAddress forwardAddress() {
		return forward_address;
	}
	
	public InetAddress externalAddress() {
		return external_address;
	}
	
	public boolean available() {
		return external_address != null;
	}
	
	public PortMapping mapPortTCP(int port) {
		return new PortMapping(this, new InetSocketAddress(forward_address, port), "TCP");
	}
	
	public PortMapping mapPortUDP(int port) {
		return new PortMapping(this, new InetSocketAddress(forward_address, port), "UDP");
	}
	
	public PortMapping.Status portStatusTCP(int port) {
		Gateway.invoke(() -> pending_ports.add(port));
		return port_status_tcp.getOrDefault(port, PortMapping.Status.UNKNOWN);
	}
	
	public PortMapping.Status portStatusUDP(int port) {
		Gateway.invoke(() -> pending_ports.add(port));
		return port_status_udp.getOrDefault(port, PortMapping.Status.UNKNOWN);
	}
	
	@Override
	public void close() {
		// setting closed flag stops the port query timer
		if (!closed.getAndSet(true)) {
			Net.invoke(() -> Net.closed(this));
		}
	}
	
	boolean init(String response) {
		try {
			BufferedReader r = new BufferedReader(new StringReader(response));
			// check response line (note that errors are not allowed, only 200 OK is valid)
			if (!r.readLine().equals("HTTP/1.1 200 OK")) return false;
			for (String line = r.readLine(); line != null; line = r.readLine()) {
				if (line.toLowerCase().startsWith("location: ")) {
					location = line.substring("location: ".length()).trim();
					// complete init async
					invoke(this::loadConfig);
					return true;
				}
			}
			return false;
		} catch (IOException e) {
			return false;
		}
	}
	
	void loadConfig() {
		try {
			logger.write(LogCategory.TRACE, "loading gateway configuration for %s", location);
			URLConnection con = new URL(location).openConnection();
			con.connect();
			BufferedReader r = new BufferedReader(new InputStreamReader(con.getInputStream()));
			StringBuilder sb = new StringBuilder();
			r.lines().forEach((String line) -> { sb.append(line); sb.append("\r\n"); });
			String text = sb.toString();
			// TODO i guess we could parse the xml properly, if we really wanted?
			for (String s : services) {
				int i0 = text.indexOf(s, 0);
				int i1 = text.indexOf("<controlURL>", Math.max(i0, 0));
				if (i0 >= 0 && i1 >= 0) {
					service = s;
					int i2 = text.indexOf("</controlURL>", i1);
					String path = text.substring(i1 + "<controlURL>".length(), i2).trim();
					String baseurl = location.substring(0, location.indexOf("/", "http://".length()));
					controlurl = baseurl + path;
					break;
				}
			}
			if (controlurl == null) {
				logger.write(LogCategory.ERROR, "failed to find gateway control url for %s", location);
				return;
			}
			// test if we can access port mapping config
			soapGetSpecificPortMappingEntry(32768, "TCP");
			// get the gateway's external address
			external_address = soapGetExternalIPAddress();
			logger.write(LogCategory.TRACE, "gateway has external address %s", external_address.getHostAddress());
			if (external_address.isSiteLocalAddress() && external_address instanceof Inet4Address) {
				Inet4Address ext4 = (Inet4Address) external_address;
				// TODO smarter gateway guessing
				byte[] b = ext4.getAddress();
				b[3] = 1;
				InetAddress next = InetAddress.getByAddress(b);
				Net.gateways().addTarget(next, external_address);
				logger.write(LogCategory.TRACE, "gateway is site-local; guessing next gateway %s", next);
			}
			// sucessfully loaded
			Net.opened(this);
			// start timer to invoke port queries
			Timer.add(PORT_DELAY, false, (long elapsed) -> {
				if (!closed.get()) {
					invoke(this::queryPorts);
					return true;
				}
				return false;
			});
		} catch (IOException e) {
			logger.write(LogCategory.ERROR, "failed to load gateway configuration for %s: %s", location, e.getMessage());
		}
		// gateway is now as configured as possible, so fire gateway discover callbacks
		Net.gateways().fireGatewayCallbacks(this);
	}
	
	// TODO refactor gateway queries to use HTTPProtocolHandler
	void soap(String action, Map<String, String> inparams, Map<String, String> outparams) throws IOException {
		// TODO outparams that arent found remain mapped to null; this may not be desirable
		//System.out.println("SOAP " + action + " to " + address);
		// prepare request body
		StringBuilder sb = new StringBuilder();
		sb.append(
			"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
				+ "SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\r\n<SOAP-ENV:Body>\r\n"
			);
		sb.append(String.format("<m:%s xmlns:m=\"%s\">\r\n", action, service));
		if (inparams != null) {
			for (Map.Entry<String, String> me : inparams.entrySet()) {
				sb.append(String.format("<%s>%s</%s>\r\n", me.getKey(), me.getValue(), me.getKey()));
			}
		}
		sb.append(String.format("</m:%s>\r\n", action));
		sb.append("</SOAP-ENV:Body>\r\n</SOAP-ENV:Envelope>\r\n");
		byte[] b = sb.toString().getBytes();
		// send http request
		HttpURLConnection con = (HttpURLConnection) new URL(controlurl).openConnection();
		con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setConnectTimeout(2000);
        con.setReadTimeout(2000);
        con.setRequestProperty("Content-Type", "text/xml");
        con.setRequestProperty("SOAPAction", String.format("\"%s#%s\"", service, action));
        con.setRequestProperty("Connection", "Close");
		con.setRequestProperty("Content-Length", "" + b.length);
		con.getOutputStream().write(b);
		int status = con.getResponseCode();
		if (status != 200 && status != 500) {
			// HTTP 500 means we have a SOAP error to parse
			throw new IOException("SOAP action " + action + " failed with http " + status);
		}
		// parse response
		StringBuilder sb2 = new StringBuilder();
		InputStream stream = status == 200 ? con.getInputStream() : con.getErrorStream();
		BufferedReader r = new BufferedReader(new InputStreamReader(stream));
		r.lines().forEach((String line) -> { sb2.append(line); sb2.append("\r\n"); });
		r.close();
		String response = sb2.toString();
		// TODO yeah, we should probably parse the xml properly
		// always add the UPnP error params so we can parse them easily
		if (outparams == null) outparams = new HashMap<>();
		outparams.put("errorCode", null);
		outparams.put("errorDescription", null);
		for (Map.Entry<String, String> me : outparams.entrySet()) {
			int i0 = response.indexOf("<" + me.getKey() + ">");
			int i1 = response.indexOf("</" + me.getKey() + ">");
			if (i0 >= 0 && i1 >= 0) {
				String s = response.substring(i0 + me.getKey().length() + 2, i1);
				me.setValue(s);
			}
		}
		String error = outparams.get("errorCode");
		if (error != null) throw new ControlException(Integer.parseInt(error), "SOAP action " + action + " failed: " + outparams.get("errorDescription"));
	}
	
	InetAddress soapGetExternalIPAddress() throws IOException {
		Map<String, String> outparams = new HashMap<>();
		outparams.put("NewExternalIPAddress", null);
		soap("GetExternalIPAddress", null, outparams);
		return InetAddress.getByName(outparams.get("NewExternalIPAddress"));
	}
	
	PortMapping.Status soapGetSpecificPortMappingEntry(int extport, String proto) throws IOException {
		Map<String, String> inparams = new HashMap<>();
		Map<String, String> outparams = new HashMap<>();
		inparams.put("NewRemoteHost", "");
		inparams.put("NewExternalPort", "" + extport);
		inparams.put("NewProtocol", proto);
		outparams.put("NewInternalPort", null);
		outparams.put("NewInternalClient", null);
		try {
			soap("GetSpecificPortMappingEntry", inparams, outparams);
		} catch (ControlException e) {
			// no such port mapping
			if (e.getUPnPError() == 714) return PortMapping.Status.AVAILABLE;
			throw e;
		}
		InetAddress client = InetAddress.getByName(outparams.get("NewInternalClient"));
		try {
			int intport = Integer.parseInt(outparams.get("NewInternalPort"));
			if (client.equals(forward_address) && extport == intport) {
				// external port mapped to the same port on a local interface
				return PortMapping.Status.MAPPED;
			}
		} catch (NumberFormatException e) {
			// NewInternalPort not present or invalid
		}
		return PortMapping.Status.UNAVAILABLE;
	}
	
	void soapAddPortMapping(InetSocketAddress client, String proto) throws IOException {
		Map<String, String> inparams = new HashMap<>();
		inparams.put("NewRemoteHost", "");
		inparams.put("NewExternalPort", "" + client.getPort());
		inparams.put("NewProtocol", proto);
		inparams.put("NewInternalPort", "" + client.getPort());
		inparams.put("NewInternalClient", client.getHostString());
		inparams.put("NewEnabled", "1");
		inparams.put("NewLeaseDuration", "0");
		inparams.put("NewPortMappingDescription", "goldeneagle");
		soap("AddPortMapping", inparams, null);
	}
	
	void soapDeletePortMapping(int port, String proto) throws IOException {
		Map<String, String> inparams = new HashMap<>();
		inparams.put("NewRemoteHost", "");
		inparams.put("NewExternalPort", "" + port);
		inparams.put("NewProtocol", proto);
		try {
			soap("DeletePortMapping", inparams, null);
		} catch (ControlException e) {
			// no such port mapping
			if (e.getUPnPError() == 714) return;
			throw e;
		}
	}

	void queryPorts() {
		for (int port : pending_ports) {
			try {
				PortMapping.Status stcp = soapGetSpecificPortMappingEntry(port, "TCP");
				PortMapping.Status sudp = soapGetSpecificPortMappingEntry(port, "UDP");
				port_status_tcp.put(port, stcp);
				port_status_udp.put(port, sudp);
			} catch (IOException e) {
				port_status_tcp.remove(port);
				port_status_udp.remove(port);
			}
		}
		// TODO clear pending ports?
		// TODO clear stale data?
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Gateway other = (Gateway) obj;
		if (address == null) {
			if (other.address != null) return false;
		} else if (!address.equals(other.address)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "Gateway[" + address + "]";
	}
	
	static void invoke(Runnable r) {
		gateway_async.invoke(r);
	}

}
