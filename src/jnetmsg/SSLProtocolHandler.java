package jnetmsg;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Objects;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLParameters;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.StaticLogSource;

import static javax.net.ssl.SSLEngineResult.HandshakeStatus;

/**
 * Experimental.
 * 
 * @author Ben Allen
 *
 */
public class SSLProtocolHandler extends WrapperProtocolHandler {

	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(SSLProtocolHandler.class, "ssl");
	
	private static AsyncExecutor ssl_async;
	private static final int SSL_DEFAULT_NET_BUFFER_SIZE = 17000;
	private static final int SSL_DEFAULT_APP_BUFFER_SIZE = 17000;
	
	static synchronized void staticInit() {
		if (ssl_async == null) {
			ssl_async = new AsyncExecutor("SSL");
		}
	}
	
	private final DynamicLogSource logger;
	private final SSLContext ssl;
	private final SSLParameters sslparams;
	private SSLEngine engine;
	private boolean read_interest = true;
	
	public SSLProtocolHandler(ProtocolHandler inner_, SSLContext ssl_, SSLParameters sslparams_) {
		// use an inner wrapper to ensure the app-side buffers can be big enough
		super(new WrapperProtocolHandler(inner_, SSL_DEFAULT_APP_BUFFER_SIZE), SSL_DEFAULT_NET_BUFFER_SIZE);
		staticInit();
		ssl = Objects.requireNonNull(ssl_);
		sslparams = Objects.requireNonNull(sslparams_);
		logger = LOGGER.withHook(connection().logger().hook());
	}
	
	@Override
	public void init() throws IOException {
		super.init();
		if (sslparams.getEndpointIdentificationAlgorithm() == null) {
			logger.write(LogCategory.WARNING, "no SSL hostname verification enabled; use SSLParameters.setEndpointIdentificationAlgorithm");
		}
		// use hostname and port (no reverse DNS!) from connect
		// this is used for SSL hostname verification!
		InetSocketAddress remote = connection().intendedRemoteAddress();
		engine = ssl.createSSLEngine(remote.getHostString(), remote.getPort());
		engine.setSSLParameters(sslparams);
		// ensure appropriate buffer sizes
		requestResizeBuffers(engine.getSession().getPacketBufferSize());
		((WrapperProtocolHandler) inner()).requestResizeBuffers(engine.getSession().getApplicationBufferSize());
		engine.setUseClientMode(connection().server() == null);
		engine.beginHandshake();
		interestWriteImmediate();
		logger.write(LogCategory.DEBUG, "SSL handshake begin");
	}
	
	@Override
	public boolean hasReadInterest() {
		return super.hasReadInterest() && read_interest;
	}
	
	@Override
	public boolean hasWriteInterest() {
		return super.hasWriteInterest();
	}
	
	@Override
	public boolean hasMoreOutAppData() {
		// if engine outbound is done, we need to ignore inner
		return outBuf().hasRemaining() || (!engine.isOutboundDone() && inner().hasMoreOutAppData());
	}
	
	@Override
	public boolean shouldCloseIn() {
		return engine.isInboundDone();
	}
	
	@Override
	public void closeIn() throws IOException {
		// flush any remaining data up to the application
		super.closeIn();
		try {
			// will throw if peer's close_notify has not been received
			engine.closeInbound();
		} catch (SSLException e) {
			// https://security.stackexchange.com/questions/82028/ssl-tls-is-a-server-always-required-to-respond-to-a-close-notify
			logger.write(LogCategory.DEBUG, "SSL close_notify not received");
		}
		// ensure we send any closing handshake stuff
		interestWriteImmediate();
	}

	@Override
	public void beginCloseOut() throws IOException {
		super.beginCloseOut();
	}
	
	@Override
	public boolean hasClosedOut() {
		// explicitly _not_ checking the inner protocol handler
		return engine.isOutboundDone() && !outBuf().hasRemaining();
	}
	
	@Override
	protected void innerClosedOut() {
		// inner protocol handler has no more data to send
		engine.closeOutbound();
	}

	private void handshakeFinished(String side) {
		String hv = sslparams.getEndpointIdentificationAlgorithm();
		logger.write(LogCategory.DEBUG, "SSL handshake finished (%s): %s, hostname %s (%s)", side, engine.getSession().getProtocol(), (hv == null ? "NOT VERIFIED" : "verified"), hv);
	}
	
	@Override
	protected boolean wrap(ByteBuffer dst, ByteBuffer src) throws IOException {
		try {
			if (engine.isOutboundDone()) {
				// SSL engine will not produce output, so do nothing
				return false;
			}
			HandshakeStatus prevhandshake = engine.getHandshakeStatus();
			SSLEngineResult r = engine.wrap(src, dst);
			//System.err.println("SSL wrap " + r.getHandshakeStatus());
			switch (r.getHandshakeStatus()) {
			case FINISHED:
				// not sure why, but we have to check if this was the first 'finish'
				if (prevhandshake != HandshakeStatus.NOT_HANDSHAKING && engine.getHandshakeStatus() == HandshakeStatus.NOT_HANDSHAKING) handshakeFinished("wrap");
				break;
			case NEED_TASK:
				for (Runnable task = engine.getDelegatedTask(); task != null; task = engine.getDelegatedTask()) {
					ssl_async.invoke(task);
				}
				// need to re-interest write after async task completion
				ssl_async.invoke(this::interestWrite);
				break;
			case NEED_UNWRAP:
			case NEED_UNWRAP_AGAIN:
				retryRead();
				break;
			default:
				break;
			}
			switch (r.getStatus()) {
			case BUFFER_OVERFLOW:
				// ensure buffers big enough for current ssl requirements
				if (requestResizeBuffers(engine.getSession().getPacketBufferSize())) {
					// now retry
					logger.write(LogCategory.DEBUG, "SSL wrap buffer resize");
					return true;
				} else {
					// need to wait for the net
					return false;
				}
			case CLOSED:
				//logger().write("SSL closed by local host");
				// need to set shutdown and closed flags
				// application can't send more data
				connection().close();
				// remaining ssl output data will be written to the network
				// and then the out buf will be empty, indicating that sending is complete
				// allowing the connection to close gracefully
				break;
			default:
				break;
			}
			return false;
		} catch (SSLException e) {
			// log more detailed ssl errors, then terminate connection
			logger.write(LogCategory.ERROR, e, "ssl error");
			throw e;
		}
	}

	@Override
	protected boolean unwrap(ByteBuffer dst, ByteBuffer src, int newbytes) throws IOException {
		try {
			if (engine.isInboundDone()) {
				// SSL engine will not consume input, so discard
				src.position(src.limit());
				return false;
			}
			HandshakeStatus prevhandshake = engine.getHandshakeStatus();
			SSLEngineResult r = engine.unwrap(src, dst);
			//System.err.println("SSL unwrap " + r.getHandshakeStatus());
			switch (r.getHandshakeStatus()) {
			case FINISHED:
				// not sure why, but we have to check if this was the first 'finish'
				if (prevhandshake != HandshakeStatus.NOT_HANDSHAKING && engine.getHandshakeStatus() == HandshakeStatus.NOT_HANDSHAKING) handshakeFinished("unwrap");
				// ensure any pending application data now gets written
				interestWriteImmediate();
				break;
			case NEED_TASK:
				for (Runnable task = engine.getDelegatedTask(); task != null; task = engine.getDelegatedTask()) {
					ssl_async.invoke(task);
				}
				// turn off read interest while waiting for tasks
				read_interest = false;
				// need to retry read after async task completion
				ssl_async.invoke(() -> {
					read_interest = true;
					retryRead();
				});
				break;
			case NEED_WRAP:
				// ensure handshake data gets written
				interestWriteImmediate();
				break;
			default:
				break;
			}
			switch (r.getStatus()) {
			case BUFFER_OVERFLOW:
				WrapperProtocolHandler inner_wrapper = (WrapperProtocolHandler) inner();
				// ensure buffers big enough for current ssl requirements
				if (inner_wrapper.requestResizeBuffers(engine.getSession().getApplicationBufferSize())) {
					// now retry
					logger.write(LogCategory.DEBUG, "SSL unwrap buffer resize");
					return true;
				} else {
					// need to wait for inner protocol handler
					return false;
				}
			case CLOSED:
				//logger().write("SSL closed by remote host");
				// shouldCloseIn() reports this to the net, causing net eof
				// the ssl engine should also now cause wrap() to observe CLOSED 
				// so we don't have to do anything here
				break;
			default:
				break;
			}
			return false;
		} catch (SSLException e) {
			// log more detailed ssl errors, then terminate connection
			logger.write(LogCategory.ERROR, e, "ssl error");
			throw e;
		}
	}
	
}
