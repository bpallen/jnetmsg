package jnetmsg.nntp;

import java.io.IOException;
import java.util.concurrent.Executor;

import jnetmsg.Connection;
import jnetmsg.NoResponseException;
import jnetmsg.Packet;
import jnetmsg.RequestResponseClient;
import jnetmsg.Result;

public class NNTPClient extends RequestResponseClient<NNTPRequest, NNTPResponse> {

	public static interface ResponseCallback extends Result.CallbackEx<NNTPResponse, IOException, NNTPClient> {}
	
	public NNTPClient(Connection con_, Executor async_) {
		super(con_, async_, null);
	}

	@Override
	protected Packet requestToPacket(NNTPRequest r) {
		return r.toPacket();
	}

	@Override
	protected NNTPResponse responseFromPacket(Packet p) {
		// note: could throw IllegalArgumentException etc
		return new NNTPResponse(p);
	}
	
	public void send(NNTPRequest r, ResponseCallback cb) throws IOException {
		super.send(NNTPClient.class, r, cb);
	}
	
	public void sendAuthInfo(String user, String pass, ResponseCallback cb) throws IOException {
		NNTPRequest r1 = new NNTPRequest("AUTHINFO USER " + user);
		NNTPRequest r2 = new NNTPRequest("AUTHINFO PASS " + pass);
		send(r1, (Result<NNTPResponse, IOException> res, NNTPClient c) -> {
			try {
				final int code = res.get().code();
				if (code == 281) {
					// no password required
					cb.execute(res, c);
				} else if (code == 381) {
					// password required
					// note send() could throw
					c.send(r2, cb);
				} else {
					// failed, abort
					cb.execute(res, c);
				}
			} catch (IOException e) {
				cb.execute(new Result<NNTPResponse, IOException>(e), c);
			}
		});
	}
	
	public void sendMode(String mode, ResponseCallback cb) throws IOException {
		NNTPRequest r = new NNTPRequest("MODE " + mode);
		send(r, cb);
	}
	
	public void sendCapabilities(ResponseCallback cb) throws IOException {
		NNTPRequest r = new NNTPRequest("CAPABILITIES");
		send(r, cb);
	}
	
	public void sendGroup(String group, ResponseCallback cb) throws IOException {
		NNTPRequest r = new NNTPRequest("GROUP " + group);
		send(r, cb);
	}
	
	public void sendQuit(ResponseCallback cb) throws IOException {
		NNTPRequest r = new NNTPRequest("QUIT");
		send(r, cb);
	}
	
}
