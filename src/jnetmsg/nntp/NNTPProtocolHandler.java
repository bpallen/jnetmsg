package jnetmsg.nntp;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

import jnetmsg.Connection;
import jnetmsg.Net;
import jnetmsg.Packet;
import jnetmsg.ProtocolHandler;
import jnetmsg.SequenceNumber;
import jnetmsg.Util;
import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.StaticLogSource;

/**
 * Experimental
 * 
 * @author Ben Allen
 *
 */
public class NNTPProtocolHandler implements ProtocolHandler {

	public static final StaticLogSource LOGGER = Net.LOGGER.newChild(NNTPProtocolHandler.class, "nntp");
	
	// unimplemented commands
	private static final HashSet<String> unimplemented = new HashSet<>();
	
	// commands that cannot be pipelined (lower case)
	private static final HashSet<String> unpipelineable = new HashSet<>();
	
	// response codes and code-command pairs that have a multiline response
	private static final HashSet<String> multiline = new HashSet<>();
	
	static {
		unimplemented.add("post");
		unimplemented.add("ihave");
		// TODO rfc3977 explicitly allows pipelining, but rfc977 makes no mention,
		// and does not seem to prohibit it. rfc2980 mentions MODE STREAM but it
		// is not clear how that relates to pipelining in general.
		unpipelineable.add("authinfo");
		unpipelineable.add("mode");
		unpipelineable.add("post");
		unpipelineable.add("ihave");
		// TODO other unpipelineable commands?
		// help
		multiline.add("100");
		// capabilities
		multiline.add("101");
		// listgroup, but not group
		multiline.add("211:listgroup");
		// list
		multiline.add("215");
		// article
		multiline.add("220");
		// head, xhdr, xzhdr
		multiline.add("221");
		// body
		multiline.add("222");
		// over, xover, xzver
		multiline.add("224");
		// hdr
		multiline.add("225");
		// newnews
		multiline.add("230");
		// newgroups
		multiline.add("231");
		// TODO other multiline responses?
		// if we don't know in advance that a message should be multiline,
		// we will treat it as singleline and read corrupted messages
	}
	
	private final DynamicLogSource logger;
	private final Connection con;
	private final boolean isclient;
	private final Queue<NNTPSequenceNumber> sequence = new LinkedList<>();
	
	private ByteBuffer in_buf = ByteBuffer.allocateDirect(1024 * 1024);
	private ByteBuffer out_buf = null;
	
	// pause sending after current packet to wait for a response?
	private boolean stalling = false;
	
	// how many packets have we received/sent?
	private int in_seq = 0;
	private int out_seq = 0;
	
	private boolean closing = false;
	
	public NNTPProtocolHandler(Connection con_) {
		con = con_;
		isclient = con.server() == null;
		// client should start stalled to wait for server hello
		stalling = isclient;
		logger = LOGGER.withHook(con.logger().hook());
	}
	
	@Override
	public Connection connection() {
		return con;
	}

	@Override
	public void init() throws IOException {
		
	}

	@Override
	public boolean hasWriteInterest() {
		return out_buf != null;
	}

	@Override
	public boolean hasMoreOutAppData() {
		return out_buf != null || peekPacket() != null;
	}

	@Override
	public ByteBuffer outBuf() {
		return out_buf;
	}

	@Override
	public ByteBuffer inBuf() {
		return in_buf;
	}

	@Override
	public void updateOut() throws IOException {
		if (out_buf != null && !out_buf.hasRemaining()) {
			// message complete
			out_buf = null;
		}
		if (out_buf != null || stalling) return;
		// packet preconditions are checked by checkSendable() from Connection.send()
		Packet p = peekPacket();
		if (p == null) return;
		if (p.isEOF()) {
			// TODO implicit QUIT command?
			collectPacket(out_seq);
			out_buf = null;
			out_seq++;
			return;
		}
		String cmd = command(p.read());
		if (unimplemented.contains(cmd)) throw new IOException("nntp " + cmd + " is unimplemented");
		// does this message need a stall before and after?
		final boolean needsync = isclient && unpipelineable.contains(cmd);
		if (needsync && !sequence.isEmpty()) {
			// stall before, try again after clearing
			stalling = true;
			//logger().write("nntp stalling before command " + cmd);
			return;
		}
		// actually begin writing
		collectPacket(out_seq);
		out_buf = p.read();
		stalling = needsync;
		//if (stalling) logger().write("nntp stalling after command " + cmd);
		out_seq++;
		NNTPSequenceNumber n = (NNTPSequenceNumber) p.sequenceNumber();
		if (n != null) {
			// record outgoing sequence numbers so we can match them with received packets
			n.command = cmd;
			sequence.offer(n);
		}
	}

	@Override
	public void updateIn(int newbytes) throws IOException {
		//logger().write("nntp in " + newbytes);
		in_buf.flip();
		try {
			while (true) {
				// find complete command line
				final int icmdend = Util.findASCII(in_buf, "\r\n");
				if (icmdend < 0) break;
				// command or response code
				String cmd = command(in_buf);
				if (unimplemented.contains(cmd)) throw new IOException("nntp " + cmd + " is unimplemented");
				// determine if message has body
				boolean hasbody = false;
				if (isclient) {
					NNTPSequenceNumber n = sequence.peek();
					if (n != null) {
						if (n.value() != in_seq - 1) {
							// client specific:
							// -1 because of initial sever hello message; we start stalled
							// so we don't send anything before receiving it. 
							// the only other 'unsequenced' server message that can happen
							// is the server closing the connection (which will either
							// match a command, or there will be no command to match), 
							// after which we should receive no more messages.
							// so, this should never actually happen.
							throw new IOException(String.format("nntp sequence failure (out=%s@%d, in=%s@%d)", n.command, n.value(), cmd, in_seq));
						}
						hasbody = multiline.contains(cmd) || multiline.contains(cmd + ":" + n.command);
						//if (hasbody) logger().write("nntp expecting multiline body for " + cmd + " response to " + n.command);
					}
				} else {
					// TODO nntp server multiline commands
				}
				ByteBuffer buf = null;
				if (hasbody) {
					// find complete body
					// first CRLF may be the end of the command
					// only examine newly received data + margin
					ByteBuffer in_buf_new = in_buf.asReadOnlyBuffer();
					// only move position forwards!
					in_buf_new.position(Math.max(in_buf_new.position(), in_buf_new.limit() - newbytes - 4));
					final int ibodyend = Util.findASCII(in_buf_new, "\r\n.\r\n");
					if (ibodyend < 0) break;
					buf = ByteBuffer.allocateDirect(ibodyend + 5 - in_buf.position());
				} else {
					// just the command
					buf = ByteBuffer.allocateDirect(icmdend + 2 - in_buf.position());
				}
				// copy message and deliver
				Util.transfer(buf, in_buf);
				buf.flip();
				if (!buf.hasRemaining()) throw new IOException("nntp packet read failed (implementation error)");
				deliverPacket(buf, isclient ? sequence.poll() : null);
				in_seq++;
				// if all responses have been read, clear any stall
				if (stalling && sequence.isEmpty()) {
					stalling = false;
					interestWriteImmediate();
					//logger().write("nntp stall cleared");
				}
			}
		} finally {
			in_buf.compact();
		}
		if (!in_buf.hasRemaining()) {
			// no space left in buffer, need to expand
			ByteBuffer buf2 = ByteBuffer.allocateDirect((int)(in_buf.capacity() * 1.5));
			logger.write(LogCategory.DEBUG, "nntp reallocating input buffer to size %d", buf2.capacity());
			in_buf.flip();
			buf2.put(in_buf);
			in_buf = buf2;
			// need to explicitly retry because all data from net may be sitting in an outer protocol handler
			retryRead();
		}
	}

	@Override
	public boolean shouldCloseIn() {
		return false;
	}

	@Override
	public void closeIn() throws IOException {
		if (stalling) throw new IOException("nntp read eof while stalling for response");
		// TODO check if eof happened during message body
	}

	@Override
	public void beginCloseOut() throws IOException {
		closing = true;
	}

	@Override
	public boolean hasClosedOut() {
		return closing && !hasMoreOutAppData();
	}

	@Override
	public boolean hasDeliveredEOF() {
		// TODO could interpret some response codes as eof
		return false;
	}

	@Override
	public void cleanup() {
		cleanupPackets();
		if (out_buf.hasRemaining()) {
			logger.write(LogCategory.WARNING, "discarded outgoing data: %d bytes", out_buf.remaining());
		}
	}

	@Override
	public void checkSendable(Packet p) {
		// TODO check command line length?
	}

	@Override
	public SequenceNumber issueSequenceNumber(Packet p) {
		return isclient ? new NNTPSequenceNumber() : null;
	}
	
	/**
	 * Extract the command (or response code) from a message (as lower case).
	 * Does not alter the buffer's state. Expects the buffer to be readable.
	 * For commands with variants, only returns the base name.
	 * 
	 * @param buf
	 * @return
	 */
	private String command(ByteBuffer buf) {
		buf = buf.asReadOnlyBuffer();
		buf.limit(Math.min(buf.position() + 512, buf.capacity()));
		int icrlf = Util.findASCII(buf, "\r\n");
		if (icrlf < 0) return null;
		buf.limit(icrlf);
		int isp = Util.findASCII(buf, " ");
		if (isp >= 0) buf.limit(isp);
		return Util.getUTF8Bytes(buf, buf.remaining()).toLowerCase();
	}

}
