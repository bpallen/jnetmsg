package jnetmsg.nntp;

import java.util.Objects;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;

import jnetmsg.Connection;
import jnetmsg.Net;
import jnetmsg.Protocol;
import jnetmsg.ProtocolHandler;
import jnetmsg.SSLProtocolHandler;

public class NNTPSProtocol implements Protocol {

	private final SSLContext ssl;
	private final SSLParameters sslparams;
	private final Protocol nntp;
	
	public NNTPSProtocol(SSLContext ssl_, SSLParameters sslparams_, Protocol nntp_) {
		ssl = Objects.requireNonNull(ssl_);
		sslparams = Objects.requireNonNull(sslparams_);
		nntp = Objects.requireNonNull(nntp_);
	}
	
	@Override
	public String name() {
		return "nntps";
	}

	@Override
	public ProtocolHandler newHandlerInstance(Connection con_) {
		return new SSLProtocolHandler(nntp.newHandlerInstance(con_), ssl, sslparams);
	}

}
