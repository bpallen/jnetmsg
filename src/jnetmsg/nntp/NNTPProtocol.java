package jnetmsg.nntp;

import jnetmsg.Connection;
import jnetmsg.Protocol;
import jnetmsg.ProtocolHandler;

public class NNTPProtocol implements Protocol {

	@Override
	public String name() {
		return "nntp";
	}

	@Override
	public ProtocolHandler newHandlerInstance(Connection con_) {
		return new NNTPProtocolHandler(con_);
	}

}
