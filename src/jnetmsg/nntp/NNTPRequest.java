package jnetmsg.nntp;

import jnetmsg.Packet;

public class NNTPRequest extends NNTPMessage {

	public NNTPRequest(String cmdline_) {
		super();
		commandLine(cmdline_);
	}
	
	public NNTPRequest(Packet p) {
		super(p);
	}
	
	public String command() {
		return commandGroup("cmd");
	}
	
	public String variant() {
		return commandGroup("variant");
	}
	
}
