package jnetmsg.nntp;

import jnetmsg.Packet;
import jnetmsg.SequenceNumber;

public class NNTPResponse extends NNTPMessage {

	private final SequenceNumber seq;
	
	public NNTPResponse(String cmdline_) {
		super();
		commandLine(cmdline_);
		seq = null;
	}
	
	public NNTPResponse(Packet p) {
		super(p);
		seq = p.sequenceNumber();
	}
	
	public SequenceNumber sequenceNumber() {
		return seq;
	}
	
	public int code() {
		return Integer.parseInt(commandGroup("code"));
	}
	
	public String codeDescription() {
		String s = commandGroup("desc");
		return s == null ? "" : s;
	}
	
}
