package jnetmsg.nntp;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jnetmsg.Packet;
import jnetmsg.Util;

public class NNTPMessage {

	private static final HashMap<String, Pattern> cmdpatterns = new HashMap<>();
	private static final Pattern default_cmdpattern = Pattern.compile("^(?:(?<code>\\d{3})|(?<cmd>\\S+))(?:\\s+(?<desc>.*))?$");
	
	static {
		Pattern p111 = Pattern.compile("^(?<code>111)\\s+(?<arg0>\\S+)(?:\\s+(?<desc>.*))?$");
		cmdpatterns.put("111", p111);
		Pattern p211 = Pattern.compile("^(?<code>211)\\s+(?<arg0>\\S+)\\s+(?<arg1>\\S+)\\s+(?<arg2>\\S+)\\s+(?<arg3>\\S+)(?:\\s+(?<desc>.*))?$");
		cmdpatterns.put("211", p211);
		Pattern p220 = Pattern.compile("^(?<code>22[0123])\\s+(?<arg0>\\S+)\\s+(?<arg1>\\S+)(?:\\s+(?<desc>.*))?$");
		cmdpatterns.put("220", p220);
		cmdpatterns.put("221", p220);
		cmdpatterns.put("222", p220);
		cmdpatterns.put("223", p220);
		Pattern p401 = Pattern.compile("^(?<code>401)\\s+(?<arg0>\\S+)(?:\\s+(?<desc>.*))?$");
		cmdpatterns.put("401", p401);
		// TODO command patterns
	}
	
	// dont include CRLF
	private String cmdline = "";
	
	// cached
	private Matcher cmdmatcher = null;
	
	// non-null => multiline
	private ByteBuffer body = null;
	
	protected NNTPMessage() {
		
	}
	
	protected NNTPMessage(Packet p) {
		ByteBuffer in = p.read();
		if (!in.hasRemaining()) throw new IllegalArgumentException("malformed nntp message (empty)");
		int icmdend = Util.findASCII(in, "\r\n");
		if (icmdend < 0) throw new IllegalArgumentException("malformed nntp message (no CRLF)");
		commandLine(Util.getUTF8Bytes(in, icmdend - in.position()));
		// check if un-dot-stuffing is needed (a lot of messages probably don't)
		final boolean needunstuff = Util.findASCII(in, "\r\n.") < (in.limit() - 5);
		// now skip cmdline CRLF
		in.position(in.position() + 2);
		if (!in.hasRemaining()) return;
		if (!Util.getASCIIBytes(in.asReadOnlyBuffer().position(in.limit() - 3), 3).equals(".\r\n")) {
			throw new IllegalArgumentException("malformed nntp message (no trailing .CRLF on body)");
		}
		// cut off the trailing .CRLF
		in.limit(in.limit() - 3);
		if (needunstuff) {
			// un-dot-stuffing can't make the data bigger
			ByteBuffer b = ByteBuffer.allocateDirect(in.remaining());
			unDotStuff(b, in);
			b.flip();
			body = b.slice().asReadOnlyBuffer();
		} else {
			body = in.slice().asReadOnlyBuffer();
		}
	}
	
	protected void commandLine(String line) {
		cmdline = line.strip();
		String cmd = cmdline.split("\\s+", 2)[0];
		Pattern pat = cmdpatterns.getOrDefault(cmd, default_cmdpattern);
		cmdmatcher = pat.matcher(cmdline);
		if (!cmdmatcher.matches()) {
			cmdmatcher = null;
			throw new IllegalArgumentException("command line did not match pattern " + pat.toString());
		}
	}
	
	protected String commandGroup(String name) {
		return cmdmatcher.group(name);
	}
	
	public String argument(int i) {
		try {
			return cmdmatcher.group("arg" + i);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
	
	public ByteBuffer body() {
		return body == null ? null : body.asReadOnlyBuffer();
	}
	
	public String decodeBody(Charset ch) {
		if (body == null) return null;
		byte[] b = new byte[body.remaining()];
		body().get(b);
		return new String(b, ch);
	}
	
	public Packet toPacket() {
		byte[] cmd = cmdline.getBytes(StandardCharsets.UTF_8);
		// if multiline: count number of dots after CRLF, +1 for possible leading dot, +3 for trailing .CRLF
		final int sz = cmd.length + 2 + (body == null ? 0 : body.remaining() + Util.countASCII(body, "\r\n.") + 1 + 3);
		ByteBuffer out = ByteBuffer.allocateDirect(sz);
		out.put(cmd);
		Util.putASCIIBytes(out, "\r\n");
		if (body != null) {
			dotStuff(out, body());
			Util.putASCIIBytes(out, ".\r\n");
		}
		out.flip();
		return new Packet(out);
	}
	
	private void dotStuff(ByteBuffer out, ByteBuffer in) {
		// must not append trailing .CRLF
		// must deal with leading .
		// check if input ends with CRLF?
		while (in.hasRemaining()) {
			in.mark();
			int iend = Util.findASCII(in, "\r\n.");
			// include CRLF in block but not .
			iend = iend < 0 ? in.limit() : iend + 2;
			final byte v0 = in.get();
			final boolean stuff = v0 == '.';
			final int c = iend - in.position();
			if (out.remaining() < c + (stuff ? 2 : 1)) {
				in.reset();
				throw new BufferOverflowException();
			}
			if (stuff) out.put((byte) '.');
			out.put(v0);
			Util.transfer(out, in, c);
		}
	}
	
	private void unDotStuff(ByteBuffer out, ByteBuffer in) {
		// must ignore trailing .CRLF
		// must deal with leading .
		while (in.hasRemaining()) {
			in.mark();
			int iend = Util.findASCII(in, "\r\n.");
			// include CRLF in block but not .
			iend = iend < 0 ? in.limit() : iend + 2;
			final byte v0 = in.get();
			if (v0 == '.') {
				if (!in.hasRemaining()) return;
				final byte v1 = in.get();
				// ignore trailing .CRLF
				if (v1 == '\r') return;
				if (v1 != '.') throw new IllegalArgumentException("invalid dot stuffing");
			}
			final int c = iend - in.position();
			if (out.remaining() < c + 1) {
				in.reset();
				throw new BufferOverflowException();
			}
			out.put(v0);
			Util.transfer(out, in, c);
		}
	}
	
}
