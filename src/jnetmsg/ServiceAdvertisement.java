package jnetmsg;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import jnetmsg.log.DynamicLogSource;
import jnetmsg.log.LogCategory;
import jnetmsg.log.LogHook;
import jnetmsg.log.StaticLogSource;

/**
 * Advertisement for a {@link ConnectionListener}. Makes the server discoverable on the local network
 * by responding to discovery requests.
 * 
 * @see Net#advertise(ConnectionListener, int, String, String)
 * @see Net#advertise(ConnectionListener, String, String)
 * 
 * @author Ben Allen
 *
 */
public class ServiceAdvertisement implements Closeable {

	public static final StaticLogSource LOGGER = Service.LOGGER.newChild(ServiceAdvertisement.class, "advertise");
	
	public static enum Status {
		STARTING, STARTED, STOPPING, STOPPED
	}
	
	public static interface StatusCallback {
		public void execute(ServiceAdvertisement advert);
	}
	
	static class Datagram {
		
		private final InetSocketAddress target;
		private final ByteBuffer payload;
		
		Datagram(InetSocketAddress target_, ByteBuffer payload_) {
			target = target_;
			payload = payload_;
		}
		
		public InetSocketAddress target() {
			return target;
		}
		
		public ByteBuffer readPayload() {
			return payload.asReadOnlyBuffer();
		}
		
	}
	
	// this is the key attachment
	// this class allows multiple advertisments on one port
	static class Proxy extends Net.ReadWriteable implements Closeable {
		
		public static final StaticLogSource LOGGER = ServiceAdvertisement.LOGGER.newChild(Proxy.class, "proxy");
		
		private final DynamicLogSource logger = LOGGER.withHook(LogHook.appendDynamicString(" [%s]", this::description));
		private final SelectionKey key;
		private final AtomicBoolean closed = new AtomicBoolean(false);
		
		// only used from net thread
		private final ByteBuffer in_buf = ByteBuffer.allocate(256);
		private final Set<ServiceAdvertisement> advertisements = new HashSet<>();
		private final Queue<Datagram> out_dgrams = new LinkedList<>();
		private final ArrayList<PortMapping> portmaps = new ArrayList<>();
		
		private final GatewayDiscovery.GatewayCallback gateway_cb = (Gateway gate) -> {
			try {
				if (!closed.get() && gate.available()) {
					int port = localAddress().getPort();
					portmaps.add(gate.mapPortUDP(port));
				}
			} catch (IOException e) {
				// idk, how would this happen anyway?
			}
		};
		
		Proxy(SelectionKey key_) {
			key = key_;
			key.attach(this);
			if (channel().isOpen()) {
				Net.opened(this);
				logger.write(LogCategory.TRACE, "start service advertisement proxy");
			}
		}
		
		String description() {
			try {
				return localAddress().toString();
			} catch (IOException e) {
				return "?";
			}
		}
		
		SelectionKey key() {
			return key;
		}
		
		DatagramChannel channel() {
			return (DatagramChannel) key.channel();
		}
		
		InetSocketAddress localAddress() throws IOException {
			return (InetSocketAddress) channel().getLocalAddress();
		}
		
		void mapGatewayPorts() {
			Net.gateways().addGatewayCallback(gateway_cb);
		}
		
		// call from net thread only
		void addAdvertisement(ServiceAdvertisement sa) {
			if (closed.get()) return;
			logger.write(LogCategory.TRACE, "start advertising service %s %s::%s", sa.server().localAddress(), sa.serviceName(), sa.instanceName());
			advertisements.add(sa);
		}
		
		// call from net thread only
		void removeAdvertisement(ServiceAdvertisement sa) {
			logger.write(LogCategory.TRACE, "stop advertising service %s %s::%s", sa.server().localAddress(), sa.serviceName(), sa.instanceName());
			advertisements.remove(sa);
		}
		
		@Override
		public void close() {
			if (closed.getAndSet(true)) return;
			if (Net.gateways() != null) Net.gateways().removeGatewayCallback(gateway_cb);
			Net.invoke(this::closeImpl);
		}
		
		private void closeImpl() {
			// ensure all ads are closed before the proxy stops
			for (ServiceAdvertisement ad : advertisements) {
				ad.close();
			}
			Net.invoke(this::cleanup);
		}
		
		private void cleanup() {
			advertisements.clear();
			logger.write(LogCategory.TRACE, "stop service advertisement proxy");
			try {
				key.cancel();
				channel().close();
			} catch (Exception e) { }
			for (PortMapping pm : portmaps) {
				pm.close();
			}
			Net.closed(this);
		}
		
		@Override
		void read() {
			DatagramChannel ch = channel();
			try {
				InetSocketAddress sender = (InetSocketAddress) ch.receive(in_buf);
				if (sender == null) return;
				in_buf.flip();
				// check magic number
				if (in_buf.getInt() != 0x65CC3CD3) return;
				// read service name
				byte[] servicebytes = new byte[0xFFFF & (int) in_buf.getShort()];
				in_buf.get(servicebytes);
				String servicename = new String(servicebytes, "UTF-8");
				// find matching services
				boolean found = false;
				for (ServiceAdvertisement sa : advertisements) {
					if (sa.serviceName().equals(servicename)) {
						found = true;
						out_dgrams.offer(new Datagram(sender, sa.readBuffer()));
					}
				}
				if (found) key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
			} catch (BufferUnderflowException e) {
				// malformed datagram, ignore
			} catch (IOException e) {
				logger.write(LogCategory.ERROR, "error reading service request: %s", e.getMessage());
				cleanup();
			} finally {
				// always clear buffer
				in_buf.clear();
			}
		}

		@Override
		void write() {
			DatagramChannel ch = channel();
			try {
				Datagram d = out_dgrams.poll();
				if (d == null) {
					// no more pending writes
					key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
					return;
				} else {
					// write a datagram
					ch.send(d.readPayload(), d.target());
				}
			} catch (IOException e) {
				logger.write(LogCategory.ERROR, "error writing service response: %s", e.getMessage());
				cleanup();
			}
		}
		
	}
	
	private final DynamicLogSource logger = LOGGER.withHook(LogHook.appendDynamicString(" [%s::%s]", this::serviceName, this::instanceName));
	private final Proxy proxy;
	private final ConnectionListener server;
	private final String service_name;
	private final String instance_name;
	private final ByteBuffer buf = ByteBuffer.allocate(256);
	private volatile Status status = Status.STARTING;
	private final AtomicBoolean closed = new AtomicBoolean(false);
	
	// only used on the net thread
	private final Set<StatusCallback> status_callbacks = new HashSet<>();
	
	private final ConnectionListener.StatusCallback listener_cb = (ConnectionListener l) -> {
		// stop advertising when listener stops
		if (l.status().ordinal() >= ConnectionListener.Status.STOPPING.ordinal()) {
			// cleanup immediately, before listener actually stops (so we can get its address)
			if (!closed.getAndSet(true)) cleanup();
		}
	};
	
	ServiceAdvertisement(Proxy proxy_, ConnectionListener server_, String service_name_, String instance_name_) throws IOException {
		proxy = proxy_;
		server = server_;
		service_name = service_name_;
		instance_name = instance_name_;
		// output buffer:
		// [4] : magic number
		// [2] : port
		// [1] : ip address length
		// [x] : ip address (0 => source address of received datagram)
		// [2] : service name length
		// [x] : service name
		// [2] : instance name length
		// [x] : instance name
		buf.putInt(0xD3A276CF);
		buf.putShort((short) server.localAddress().getPort());
		buf.put((byte) 4);
		buf.putInt(0);
		byte[] servicebytes = service_name.getBytes("UTF-8");
		buf.putShort((short) servicebytes.length);
		buf.put(servicebytes);
		byte[] instancebytes = instance_name.getBytes("UTF-8");
		buf.putShort((short) instancebytes.length);
		buf.put(instancebytes);
		buf.flip();
		server.addStatusCallback(listener_cb);
		Net.invoke(() -> proxy.addAdvertisement(this));
		Net.opened(this);
		// Status.STARTING isn't actually used
		status(Status.STARTED);
	}
	
	private void execCallback(StatusCallback cb) {
		try {
			cb.execute(this);
		} catch (RuntimeException e) {
			logger.write(LogCategory.ERROR, e, "exception in status callback");
		}
	}
	
	ByteBuffer readBuffer() {
		return buf.asReadOnlyBuffer();
	}
	
	/**
	 * @return Status of this service advertisment
	 */
	public Status status() {
		return status;
	}
	
	private void status(Status s) {
		Status old = status;
		status = s;
		if (s != old) {
			for (StatusCallback cb : status_callbacks) {
				execCallback(cb);
			}
		}
	}
	
	/**
	 * @return Advertised server
	 */
	public ConnectionListener server() {
		return server;
	}
	
	/**
	 * @return Name of the advertised service
	 */
	public String serviceName() {
		return service_name;
	}
	
	/**
	 * @return Name of this instance of the advertised service
	 */
	public String instanceName() {
		return instance_name;
	}
	
	/**
	 * @return Local socket address of the advertisement proxy
	 * @throws IOException
	 */
	public InetSocketAddress localAddress() throws IOException {
		return proxy.localAddress();
	}
	
	public void mapGatewayPorts() {
		proxy.mapGatewayPorts();
	}
	
	private void cleanup() {
		status(Status.STOPPING);
		server.removeStatusCallback(listener_cb);
		proxy.removeAdvertisement(this);
		Net.closed(this);
		status(Status.STOPPED);
	}
	
	/**
	 * Close this service advertisement. It will no longer be found by service discovery.
	 */
	@Override
	public void close() {
		if (closed.getAndSet(true)) return;
		Net.invoke(this::cleanup);
	}
	
	/**
	 * Add a status callback to this service advertisement. The callback will be called
	 * whenever the status of the advertisement is changed.
	 * 
	 * If the status when the callback is added is not {@link Status#STARTING},
	 * the callback is called as soon as possible.
	 * 
	 * Status callbacks are always called from the Net worker thread.
	 * 
	 * @param cb Callback to add
	 */
	public void addStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			if (status_callbacks.add(cb) && status != Status.STARTING) {
				execCallback(cb);
			}
		});
	}
	
	/**
	 * Remove a status callback
	 * 
	 * @param cb Callback to remove
	 */
	public void removeStatusCallback(StatusCallback cb) {
		Net.invoke(() -> {
			status_callbacks.remove(cb);
		});
	}
	
	@Override
	public String toString() {
		try {
			return "ServiceAdvertisement[local " + localAddress() + ", " + server +
				", '" + service_name + "'::'" + instance_name + "']"; 
		} catch (IOException e) {
			return "ServiceAdvertisement[?]";
		}
	}
}
