package jnetmsg;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AsyncTicker {

	private final Executor async;
	private final AtomicInteger pending_ticks = new AtomicInteger(0);
	
	public AsyncTicker(Executor async_) {
		async = async_;
	}
	
	public Executor executor() {
		return async;
	}
	
	protected void fireTick() {
		if (pending_ticks.getAndIncrement() == 0) {
			if (async != null) async.execute(this::runTicks);
		}
	}
	
	/**
	 * @return Object that {@link #runTicks()} is <code>synchronized()</code> on.
	 */
	protected Object tickSync() {
		return pending_ticks;
	}
	
	protected void runTicks() {
		// synchronizing not on this to avoid interfering with subclass synchronization
		synchronized (tickSync()) {
			int ticks = 0;
			do {
				ticks = pending_ticks.get();
				if (ticks < 1) return;
				try {
					tick(ticks);
				} catch (RuntimeException e) {
					System.err.println("exception in async tick");
					e.printStackTrace();
				}
			} while (pending_ticks.addAndGet(-ticks) > 0);
			// this should be the only thing decrementing pending_ticks (and is synchronized)
		}
	}
	
	protected abstract void tick(int ticks);
	
}
