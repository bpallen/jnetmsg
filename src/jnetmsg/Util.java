package jnetmsg;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * 
 * @author Ben Allen
 *
 */
public class Util {

	public static final ByteBuffer EMPTY_BUFFER = ByteBuffer.allocate(0);
	
	public static int find(ByteBuffer whole, ByteBuffer subseq) {
		if (!subseq.hasRemaining()) return whole.position();
		// TODO smarter search
		// slice to get position at 0
		subseq = subseq.slice();
		for (int i = whole.position(); i <= whole.limit() - subseq.remaining(); i++) {
			int j = 0;
			for (; j < subseq.limit(); j++) {
				if (whole.get(i + j) != subseq.get(j)) break;
			}
			if (j == subseq.limit()) return i;
		}
		return -1;
	}
	
	public static int findASCII(ByteBuffer whole, String subseq) {
		return find(whole, ByteBuffer.wrap(subseq.getBytes(StandardCharsets.US_ASCII)));
	}
	
	public static int findUTF8(ByteBuffer whole, String subseq) {
		return find(whole, ByteBuffer.wrap(subseq.getBytes(StandardCharsets.UTF_8)));
	}
	
	public static int count(ByteBuffer whole, ByteBuffer subseq) {
		if (!subseq.hasRemaining()) return whole.position();
		int c = 0;
		// TODO smarter search
		// slice to get position at 0
		subseq = subseq.slice();
		for (int i = whole.position(); i <= whole.limit() - subseq.remaining(); i++) {
			int j = 0;
			for (; j < subseq.limit(); j++) {
				if (whole.get(i + j) != subseq.get(j)) break;
			}
			if (j == subseq.limit()) c++;
		}
		return c;
	}
	
	public static int countASCII(ByteBuffer whole, String subseq) {
		return count(whole, ByteBuffer.wrap(subseq.getBytes(StandardCharsets.US_ASCII)));
	}
	
	public static int countUTF8(ByteBuffer whole, String subseq) {
		return count(whole, ByteBuffer.wrap(subseq.getBytes(StandardCharsets.UTF_8)));
	}
	
	public static void putASCIIBytes(ByteBuffer out, String s) {
		out.put(s.getBytes(StandardCharsets.US_ASCII));
	}
	
	public static void putUTF8Bytes(ByteBuffer out, String s) {
		out.put(s.getBytes(StandardCharsets.UTF_8));
	}
	
	public static String getASCIIBytes(ByteBuffer in, int count) {
		byte[] b = new byte[count];
		in.get(b);
		return new String(b, StandardCharsets.US_ASCII);
	}
	
	public static String getUTF8Bytes(ByteBuffer in, int count) {
		byte[] b = new byte[count];
		in.get(b);
		return new String(b, StandardCharsets.UTF_8);
	}
	
	public static String getASCIIBytes(ByteBuffer in, String delim) {
		ByteBuffer q = ByteBuffer.wrap(delim.getBytes(StandardCharsets.US_ASCII));
		int iend = find(in, q);
		if (iend < 0) return null;
		int c = iend - in.position();
		String s = getASCIIBytes(in, c);
		// also consume delim
		in.position(in.position() + q.remaining());
		return s;
	}
	
	public static String getUTF8Bytes(ByteBuffer in, String delim) {
		ByteBuffer q = ByteBuffer.wrap(delim.getBytes(StandardCharsets.UTF_8));
		int iend = find(in, q);
		if (iend < 0) return null;
		int c = iend - in.position();
		String s = getASCIIBytes(in, c);
		// also consume delim
		in.position(in.position() + q.remaining());
		return s;
	}
	
	public static int transferImpl(ByteBuffer dst, ByteBuffer src, int c) {
		ByteBuffer src2 = src.asReadOnlyBuffer();
		src2.limit(src2.position() + c);
		dst.put(src2);
		src.position(src.position() + c);
		return c;
	}
	
	public static int transfer(ByteBuffer dst, ByteBuffer src) {
		final int c = Math.min(dst.remaining(), src.remaining());
		if (c == 0) return 0;
		return transferImpl(dst, src, c);
	}
	
	public static int transfer(ByteBuffer dst, ByteBuffer src, int limit) {
		final int c = Math.min(Math.min(dst.remaining(), src.remaining()), Math.max(limit, 0));
		if (c == 0) return 0;
		return transferImpl(dst, src, c);
	}
	
}
