package jnetmsg;

import java.time.Duration;
import java.time.Instant;

/**
 * Provides a means to associate responses to earlier requests.
 * 
 * @author Ben Allen
 *
 */
public class SequenceNumber {

	// set from protocol handler
	int value = -1;
	
	// set from connection
	Instant time_sent = null;
	Instant time_rcvd = null;
	
	/**
	 * Field for a user-specified object that can be set after a call to
	 * {@link Connection#send(Packet)} and then later retrieved after a call
	 * to {@link Connection#receive()}. Useful for e.g. a callback.
	 */
	public Object user;
	
	public SequenceNumber() {
		
	}
	
	/**
	 * Get the value of this sequence number. This value is only required to be
	 * determined after the {@link Packet} with this sequence number is returned
	 * from {@link Connection#receive()}. The values themselves, when determined,
	 * are required to be non-negative and monotonically increasing w.r.t. successive
	 * calls to {@link Connection#send(Packet)} (from the same thread), and must not
	 * repeat, but are not required to be contiguous.
	 * 
	 * @return Actual sequence number or <code>-1</code> if not yet determined
	 */
	public int value() {
		return value;
	}
	
	/**
	 * Time the request was sent (when the packet was collected by the protocol).
	 *   
	 * @return time or null
	 */
	public Instant timeRequestSent() {
		return time_sent;
	}
	
	/**
	 * Time the response was received (when the packet was delivered by the protocol).
	 * 
	 * @return time or null
	 */
	public Instant timeResponseReceived() {
		return time_rcvd;
	}
	
	/**
	 * @return Duration between sending the request and receiving the response, or null.
	 */
	public Duration responseDelay() {
		if (time_sent == null || time_rcvd == null) return null;
		return Duration.between(time_sent, time_rcvd);
	}
	
	@Override
	public final int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public final boolean equals(Object other) {
		return super.equals(other);
	}
	
}
