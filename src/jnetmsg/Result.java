package jnetmsg;

import java.util.Objects;
import java.util.Optional;

public class Result<T, E extends Exception> {

	public static interface Callback<T, E extends Exception> {
		public void execute(Result<T, E> res);
	}
	
	public static interface CallbackEx<T, E extends Exception, C> {
		public void execute(Result<T, E> res, C ctx);
	}
	
	public static interface Producer<T, E extends Exception> {
		public T evaluate() throws E;
	}
	
	private T value;
	private Exception error;
	private Producer<T, ? extends E> producer;
	
	public Result() {}
	
	public Result(T value_) {
		value = Objects.requireNonNull(value_);
	}
	
	public Result(E error_) {
		error = Objects.requireNonNull(error_);
	}
	
	public Result(RuntimeException error_) {
		error = Objects.requireNonNull(error_);
	}
	
	public Result(Producer<T, ? extends E> producer_) {
		producer = Objects.requireNonNull(producer_);
	}
	
	/**
	 * @return true iff a call to {@link #get()} would throw
	 */
	public boolean failed() {
		if (error != null) return true;
		if (value != null) return false;
		try {
			get();
			return false;
		} catch (Exception e) {
			return true;
		}
	}
	
	public T getOrDefault(T defval) {
		try {
			return get();
		} catch (Exception e) {
			return defval;
		}
	}
	
	public Optional<T> tryGet() {
		try {
			return Optional.of(get());
		} catch (Exception e) {
			return Optional.empty();
		}
	}
	
	/**
	 * Get value, possibly lazily evaluating it.
	 * @throws E if stored or produced by lazy evaluation.
	 * @throws NullPointerException if no value and no other error.
	 * 
	 * @return stored or lazily computed value
	 */
	public T get() throws E {
		if (value != null) return value;
		if (error != null) rethrow();
		if (producer != null) {
			// only try to eval the producer once, even if it returns null or throws
			Producer<T, ? extends E> p = producer;
			producer = null;
			try {
				value = p.evaluate();
			} catch (Exception e) {
				error = e;
			}
			return get();
		}
		throw new NullPointerException();
	}
	
	@SuppressWarnings("unchecked")
	private void rethrow() throws E {
		// if error is not an E, it should only be a RuntimeException
		// TODO what happens to the original stacktrace?
		Objects.requireNonNull(error);
		throw (E) error;
	}
	
}
