package jnetmsg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

/**
 * Output stream that writes to an intermediate buffer and provides easy conversion
 * to a {@link Packet}.
 * 
 * @author Ben Allen
 *
 */
public class PacketOutputStream extends DataOutputStream {

	/**
	 * Construct a new PacketOutputStream.
	 */
	public PacketOutputStream() {
		super(new ByteArrayOutputStream());
	}
	
	/**
	 * @return Packet representation of the data written to this output stream
	 */
	public Packet toPacket() {
		return new Packet(((ByteArrayOutputStream) out).toByteArray());
	}
}
