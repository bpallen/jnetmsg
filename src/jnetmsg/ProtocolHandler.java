package jnetmsg;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import jnetmsg.log.LogCategory;
import jnetmsg.log.LogSource;
import jnetmsg.log.StaticLogSource;

/**
 * Implements a protocol from the point of view of the network.
 * Except where stated, all methods must be called only from the net thread
 * and so do not require thread safety. 
 * 
 * @author Ben Allen
 * 
 */
public interface ProtocolHandler {
	
	/**
	 * @return The connection this protocol handler is working for
	 */
	public Connection connection();
	
	/**
	 * Deprecated because protocol handlers should use their own log source (with the connection's log hook).
	 * However, we can't instantiate a dynamic log source for this class because this is an interface.
	 * 
	 * @return The connection's {@link Logger}
	 */
	@Deprecated
	default LogSource logger() {
		return connection().logger();
	}
	
	/**
	 * Implementations should use this to deliver a packet to the application.
	 * Sequence number objects must be the same as those obtained from
	 * {@link #collectPacket(int)} or null <i>only</i>. 
	 * 
	 * @param buf
	 * @param seq
	 */
	default void deliverPacket(ByteBuffer buf, SequenceNumber seq) {
		connection().deliverPacketImpl(Objects.requireNonNull(buf), seq);
	}
	
	/**
	 * Implementations should use this to deliver an EOF packet
	 * ({@link Packet#isEOF()}) to the application.
	 * Must not be called more than once.
	 */
	default void deliverEOF() {
		connection().deliverPacketImpl(null, null);
	}
	
	/**
	 * Implementations can use this to peek at the next {@link Packet} that will
	 * be returned by {@link #collectPacket(int)}.
	 * 
	 * @return The {@link Packet} or null if none waiting
	 */
	default Packet peekPacket() {
		return connection().outgoing_packets.peek();
	}
	
	/**
	 * Implementations should use this to collect a packet from the application.
	 * When the application has finished sending packets, will return a single
	 * packet where {@link Packet#isEOF()}, then null indefinitely.
	 * Note that if null is returned, the provided sequence value
	 * <i>was not used</i>!.
	 * 
	 * @param seq {@link SequenceNumber} value to be assigned if required
	 * @return The {@link Packet}, or null if none waiting
	 */
	default Packet collectPacket(int seq) {
		return connection().collectPacketImpl(seq);
	}
	
	/**
	 * Implementations can use this to collect and discard all available packets
	 * from the application.
	 * 
	 * @return Number of packets discarded
	 */
	default int cleanupPackets() {
		int c = 0;
		for (Packet p = collectPacket(-1); p != null; p = collectPacket(-1)) {
			c++;
		}
		if (c > 0) logger().write(LogCategory.WARNING, "discarded outgoing data: %d packets", c);
		return c;
	}
	
	/**
	 * Implementations can use this to signal that they have data needing to
	 * be written to the network. Can be called from any thread.
	 */
	default void interestWrite() {
		connection().interestWrite();
	}
	
	/**
	 * Implementations can use this to signal that they have data needing to
	 * be written to the network. Net thread only.
	 */
	default void interestWriteImmediate() {
		connection().interestWriteImmediate();
	}
	
	/**
	 * Implementations can use this to signal that they want to retry reading
	 * data from the network, even if no data is actually available to be read,
	 * e.g. to flush stagnant data up towards the application.
	 * This is <b>not</b> required in order to read new data from the network.
	 * Can be called from any thread.
	 */
	default void retryRead() {
		connection().retryRead();
	}
	
	/**
	 * Implementations can use this to overwrite the value of a {@link SequenceNumber}.
	 * Prefer using {@link #collectPacket(int)} if at all possible.
	 * 
	 * @param n
	 * @param v
	 */
	default void sequenceValue(SequenceNumber n, int v) {
		n.value = v;
	}
	
	/**
	 * Called after the connection's channel has successfully connected,
	 * and before and network read or write operations are attempted.
	 * Protocol initialization can be initiated here (handshake etc).
	 */
	public void init() throws IOException;
	
	/**
	 * Determine if this protocol handler is expecting to consume more input in the
	 * immediate future. Called before {@link #updateIn(int)} but <i>after</i> attempting
	 * to produce data into {@link #inBuf()} (so that the state of the buffer may be used
	 * to determine read interest). If false, network read interest may be removed for
	 * this connection. If false is returned, this protocol handler <b>MUST</b> have a
	 * means to restore read interest (via {@link #retryRead()}). It is therefore
	 * recommended not to override this unless there is something actively preventing
	 * this protocol handler from consuming input.
	 * 
	 * @return true if immediate read interest
	 */
	default boolean hasReadInterest() {
		return true;
	}
	
	/**
	 * Determine if this protocol handler is expecting to produce more output in the 
	 * immediate future. Called after {@link #updateOut()} but <i>before</i> attempting to
	 * consume data from {@link #outBuf()} (so that the state of the buffer may be used
	 * to determine write interest). If false, network write interest may be removed
	 * for this connection.
	 * 
	 * @return true if immediate write interest
	 */
	public boolean hasWriteInterest();
	
	/**
	 * Determine if this protocol handler has consumed all available data from the application
	 * (via {@link #collectPacket()}) <b>and</b> all corresponding output has been consumed.
	 * A return value of <code>true</code> is not required to be reliable, but <code>false</code>
	 * <i>must</i> indicate that, given no additional data or commands sent from the application
	 * or received from the network, no output remains to be consumed from this protocol handler.
	 * 
	 * No particular state is subsequently required of {@link #outBuf()}.
	 * 
	 * @return false if no more app data remains to be consumed
	 */
	public boolean hasMoreOutAppData();
	
	/**
	 * Get the source buffer for data to be sent to the network (outgoing).
	 * {@link #updateOut()} will be called immediately before this is used.
	 * 
	 * @return The buffer or null
	 */
	public ByteBuffer outBuf();
	
	/**
	 * Get the destination buffer for data to be read from the network (incoming).
	 * If null, no data will be read but {@link #updateIn()} will still be called.
	 * 
	 * @return The buffer or null 
	 */
	public ByteBuffer inBuf();
	
	/**
	 * Called before data is attempted to be consumed from {@link #outBuf()}.
	 * 
	 * @throws IOException
	 */
	public void updateOut() throws IOException;
	
	/**
	 * Called after data has been attempted to be produced into {@link #inBuf()}.
	 * 
	 * @param newbytes Upper bound on the number of new bytes since the previous
	 * invocation (non-negative)
	 * @throws IOException
	 */
	public void updateIn(int newbytes) throws IOException;
	
	/**
	 * Called after network read operations to determine if more input will
	 * be consumed by this protocol handler. If true, the network input will
	 * be shutdown and {@link #closeIn()} will subsequently be called.
	 * 
	 * @return true iff network input should be shutdown
	 * @throws IOException
	 */
	public boolean shouldCloseIn();
	
	/**
	 * Called when the network stream has been read to EOF.
	 * As much pending data as possible should be forwarded to the application,
	 * as this is the last chance to do so. 
	 * 
	 * @throws IOException
	 */
	public void closeIn() throws IOException;
	
	/**
	 * Called at most once to begin closure procedure.
	 * Write interest will be instated (once) after calling this.
	 * 
	 * <p><b>Prerequisite</b>: Expects that all available outgoing application data has
	 * been consumed from this protocol handler (i.e. {@link #hasMoreOutAppData()}
	 * <code>== false</code>) and that the application has finished sending packets.</p>
	 * 
	 * @throws IOException
	 */
	public void beginCloseOut() throws IOException;
	
	/**
	 * Determine if all outgoing data has been consumed from this protocol handler.
	 * If true, no more data will ever need consuming. In this case, any data remaining
	 * in {@link #outBuf()} may not be consumed.
	 * 
	 * @return
	 * @throws IOException
	 */
	public boolean hasClosedOut();
	
	/**
	 * @return true iff an EOF packet ({@link Packet#isEOF()}) has already been delivered
	 * ({@link #deliverEOF()}) to the application by this protocol handler
	 */
	public boolean hasDeliveredEOF();
	
	/** 
	 * Called after all network i/o has finished.
	 * Implementations can use this to report 'dirty' closure, discarded packets etc.
	 */
	public void cleanup();
	
	/**
	 * Check that a packet can be successfully sent.
	 * Intended to catch things like oversize packets,
	 * and not to perform e.g. syntax validation.
	 * 
	 * Must ignore {@link Packet#connection()} and {@link Packet#sequenceNumber()}.
	 * Must be callable from any thread.
	 * 
	 * @param p
	 * @throws IllegalArgumentException if the packet is not sendable
	 */
	public void checkSendable(Packet p);
	
	/**
	 * Issue a {@link SequenceNumber} for an outgoing {@link Packet}.
	 * Should not resolve the sequence number's value, instead deferring that
	 * until the packet is collected ({@link #collectPacket()}) on the net thread.
	 * May return null if the protocol is not expecting to associate this packet
	 * with a response. May return an instance of a subclass.
	 * 
	 * Must ignore {@link Packet#connection()} and {@link Packet#sequenceNumber()}.
	 * Must be callable from any thread.
	 * 
	 * @param p Outgoing {@link Packet}
	 * @return {@link SequenceNumber} or null
	 */
	public SequenceNumber issueSequenceNumber(Packet p);
	
}
