package jnetmsg;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * A packet of data received or sent across the network.
 * 
 * To construct a packet, use a {@link java.nio.ByteBuffer} or {@link PacketOutputStream} for an output stream interface.
 * 
 * @author Ben Allen
 *
 */
public class Packet {

	ByteBuffer buf;
	Connection con;
	SequenceNumber seq;
	
	Packet(ByteBuffer buf_, Connection con_, SequenceNumber seq_) {
		buf = buf_;
		con = con_;
		seq = seq_;
	}
	
	/**
	 * Construct a packet wrapping a byte buffer. The packet's size is <code>b.remaining()</code>,
	 * from <code>b.position()</code> to <code>b.limit()</code>.  
	 * 
	 * Changes to the original buffer's position/limit etc will not be reflected by the packet,
	 * but modifications to its data will. 
	 * 
	 * @param b Byte buffer
	 */
	public Packet(ByteBuffer b) {
		this(Objects.requireNonNull(b).asReadOnlyBuffer(), null, null);
	}
	
	/**
	 * Construct a packet wrapping a byte array.
	 * 
	 * @param a Byte array
	 */
	public Packet(byte[] a) {
		buf = ByteBuffer.wrap(a);
	}
	
	/**
	 * Construct a packet from a string encoded as UTF-8.
	 * 
	 * @param s String to encode
	 */
	public static Packet encodeString(String s) {
		return new Packet(ByteBuffer.wrap(s.getBytes(StandardCharsets.UTF_8)));
	}
	
	/**
	 * @return {@link Connection} this packet was received from, or null
	 */
	public Connection connection() {
		return con;
	}
	
	/**
	 * @return {@link SequenceNumber} this packet was received for, or null
	 */
	public SequenceNumber sequenceNumber() {
		return seq;
	}
	
	/**
	 * @return True if this packet marks EOF for the connection it was received from
	 * @see Connection#isEOF()
	 */
	public boolean isEOF() {
		return buf == null;
	}
	
	/**
	 * @return Count of bytes in this packet (or 0 if {@link #isEOF()})
	 */
	public int size() {
		if (isEOF()) return 0;
		return buf.remaining();
	}
	
	/**
	 * Returns a read-only byte buffer wrapping the packet's internal buffer.
	 * 
	 * Changes to the position/limit etc of the returned buffer do not affect
	 * the internal buffer.
	 * 
	 * @return Read-only wrapper buffer
	 * @throws IllegalStateException if {@link #isEOF()}
	 */
	public ByteBuffer read() {
		if (isEOF()) throw new IllegalStateException("packet is eof");
		return buf.asReadOnlyBuffer();
	}
	
	/**
	 * Returns a byte array representation of the packet's internal buffer.
	 * 
	 * This will return the array backing the internal buffer if possible,
	 * and a copy of the portion readable through the buffer if not.
	 * 
	 * As such, it is not safe to write to the returned array.
	 * 
	 * @return Byte array
	 * @throws IllegalStateException if {@link #isEOF()}
	 */
	public byte[] readArray() {
		if (isEOF()) throw new IllegalStateException("packet is eof");
		if (buf.hasArray() && buf.position() == 0 && buf.limit() == buf.capacity()) {
			return buf.array();
		} else {
			byte[] a = new byte[buf.remaining()];
			read().get(a);
			return a;
		}
	}
	
	/**
	 * Returns a stream representation of the packet's internal buffer.
	 * 
	 * Reading from this stream does not affect the internal buffer.
	 * 
	 * @return Wrapper input stream
	 */
	public DataInputStream readStream() {
		return new DataInputStream(new ByteArrayInputStream(readArray()));
	}
	
	/**
	 * @return Contents of this packet decoded as a UTF-8 string or an empty string if <code>.isEOF()</code>
	 */
	public String decodeString() {
		if (isEOF()) return "";
		try {
			return new String(readArray(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new AssertionError(e);
		}
	}
	
	@Override
	public String toString() {
		return "Packet[" + (isEOF() ? "EOF]" : "" + buf.remaining() + "B]");
	}
}
